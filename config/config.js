const path = require('path');
const rootPath = path.normalize(__dirname + '/..');
const env = process.env.NODE_ENV || 'development';

const config = {
  development: {
    root: rootPath,
    app: {
      name: 'es-fe'
    },
    port: process.env.PORT || 3003,
  },

  test: {
    root: rootPath,
    app: {
      name: 'es-fe'
    },
    port: process.env.PORT || 3003,
  },

  production: {
    root: rootPath,
    app: {
      name: 'es-fe'
    },
    port: process.env.PORT || 3003,
  }
};

module.exports = config[env];
