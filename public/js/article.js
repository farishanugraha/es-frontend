var totalDataContainer = $('#output_totalposts');
var allDataContainer = $('#nexts');
var titleContainer = $('#output_title');
var dateContainer = $('#output_date');
var contentContainer = $('#output_content');
var mediaContainer = $('#output_media');

$(document).ready(function(){
	showData('latest')
	getAllData();
	dataController();
});

function showData(id){
	if(id=='latest'){
		getData(url_wp_soekamti.posts, function(json){	
			var data = json[0];
			getData(url_wp_soekamti.media+data.featured_media, function(data){
				var src = data.media_details.sizes.medium_large.source_url;
				mediaContainer.html('<img src="'+src+'">');
			});
			titleContainer.html(data.title.rendered);
			dateContainer.html('<i>tanggal: '+data.date+'</i>');
			contentContainer.html(data.content.rendered);
		})
	}
	else{
		getData(url_wp_soekamti.posts+id, function(data){
			console.log(data.featured_media)
			if(data.featured_media==undefined){
				console.log('media undefined')
			}else{
				getData(url_wp_soekamti.media+data.featured_media, function(data){
					var src = data.media_details.sizes.medium_large.source_url;
					mediaContainer.html('<img src="'+src+'">');
				});
			}
			titleContainer.html(data.title);
			dateContainer.html('<i>tanggal: '+data.date+'</i>');

			if(data.content==undefined){
				contentContainer.html(data);
			}else{
				contentContainer.html(data.content.rendered);
			}
		})
	}

	getData(url_wp_soekamti.posts, function(json){
		totalDataContainer.html('total data: '+json.length);	
	});
}

function getAllData(wp){
	getData(url_wp_soekamti.posts, function(json){
		for (var i = 0; i <= json.length - 1; i++) {
			var data = json[i];
			var id = data.id;
			var title = data.title.rendered;
			var excerpt = data.excerpt.rendered;
			if(i<=3){
				$('#others_container').append('<li id="'+id+'"><p class="bold title">'+title+'</p>'+excerpt+'</li>')
			}else{
				allDataContainer.append('<li id="'+id+'"><p class="bold">'+title+'</p>');
			}
		}
	})
}

function dataController(){
	$('#others_container').click(function(e){
		showData(e.target.parentNode.id);
	})
	allDataContainer.click(function(e){
		if(e.target.parentNode.id=='nexts'){
			showData(e.target.id);
		}else{
			showData(e.target.parentNode.id);
		}
	})
}