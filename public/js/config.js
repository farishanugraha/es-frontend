// console.log('%c config.js ready', 'color:green');

var url = {
    "base"     : "https://api.erixsoekamti.com/v1/",
    "login"    : "https://api.erixsoekamti.com/v1/login",
    "forgot"   : "https://api.erixsoekamti.com/v1/forgot",
    "register" : "https://api.erixsoekamti.com/v1/register",
    "user"     : "https://api.erixsoekamti.com/v1/user/",
    "token"    : "https://api.erixsoekamti.com/v1/checktoken",
    "resetpass": "https://api.erixsoekamti.com/v1/reset-password",
    "feedback" : "https://api.erixsoekamti.com/v1/user/feedback",
    "video" : "https://api.erixsoekamti.com/v1/user/feedback/video",
    // "upload"    : "https://api.erixsoekamti.com/v1/upload"
    "upload"    : "http://192.168.2.220:3000/v1/upload"
}

var url_wp_soekamti = {
    "base" : 'http://wp.soekamti.com/wp-json/wp/v2/',
    "posts" : 'http://wp.soekamti.com/wp-json/wp/v2/posts/',
    "media" : 'http://wp.soekamti.com/wp-json/wp/v2/media/'
}

var wp_categories = {
    '1': 'uncategorized',
    '2': 'amphi',
    '3': 'bands',
    '4': 'personal',
    '5': 'garage',
    '6': 'pool',
    '7': 'kitchen',
    '8': 'children',
    '9': 'guest',
    '10': 'family',
    '12': 'office',
    '13': 'visual',
    '14': 'garden',
    '16': 'gym',
    '17': 'pendopo'
}

var url_wp_es = {
    "base" : 'https://article.erixsoekamti.com/wp-json/wp/v2/',
    "posts" : 'https://article.erixsoekamti.com/wp-json/wp/v2/posts/',
    "categories" : 'https://article.erixsoekamti.com/wp-json/wp/v2/categories/',
    "uncategorized" : 'https://article.erixsoekamti.com/wp-json/wp/v2/posts?categories=1',
    "amphi" : 'https://article.erixsoekamti.com/wp-json/wp/v2/posts?categories=2',
    "bands" : 'https://article.erixsoekamti.com/wp-json/wp/v2/posts?categories=9',
    "personal" : 'https://article.erixsoekamti.com/wp-json/wp/v2/posts?categories=4',
    "garage" : 'https://article.erixsoekamti.com/wp-json/wp/v2/posts?categories=5',
    "pool" : 'https://article.erixsoekamti.com/wp-json/wp/v2/posts?categories=6',
    "kitchen" : 'https://article.erixsoekamti.com/wp-json/wp/v2/posts?categories=7',
    "children" : 'https://article.erixsoekamti.com/wp-json/wp/v2/posts?categories=8',
    "guest" : 'https://article.erixsoekamti.com/wp-json/wp/v2/posts?categories=3',
    "family" : 'https://article.erixsoekamti.com/wp-json/wp/v2/posts?categories=10',
    "office" : 'https://article.erixsoekamti.com/wp-json/wp/v2/posts?categories=12',
    "visual" : 'https://article.erixsoekamti.com/wp-json/wp/v2/posts?categories=13',
    "garden" : 'https://article.erixsoekamti.com/wp-json/wp/v2/posts?categories=14',
    "gym" : 'https://article.erixsoekamti.com/wp-json/wp/v2/posts?categories=16',
    "pendopo" : 'https://article.erixsoekamti.com/wp-json/wp/v2/posts?categories=17'
}

var rooms = {
	"home"     : "/",
    "main"     : "/main",
    "login"     : "/login",
    "register"     : "/register",
    "forgot_pass"     : "/forgot_pass",
    "upgrade"     : "/upgrade/",
    "event"     : "/event",
    "confirm"     : "/confirm",
    "does"     : "/does",
    "schedule"     : "/schedule"
}

var loader = '<div class="loader loader--style1" title="0"> <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"> <path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946 s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634 c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"/> <path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0 C22.32,8.481,24.301,9.057,26.013,10.047z"> <animateTransform attributeType="xml"attributeName="transform"type="rotate"from="0 20 20"to="360 20 20"dur="0.5s"repeatCount="indefinite"/> </path> </svg> </div>';

var curUserRaw = localStorage.getItem('curUser');
var curUser = JSON.parse(curUserRaw);

var cities;
var urlCityRaw = "https://api.blood4life.id/v1/provinces/c/";
var urlCity;
var provinces;
var urlProv = "https://api.blood4life.id/v1/provinces";
//=====================================================================
// console.log(url)

var rooms_preview = [
    {
        "room" : "main",
        "desc" : "<h3>Ruang Tamu</h3>Nikmati artikel, foto, dan fitur-fitur gratis. Tulis pesan dan kesanmu."
    },
    {
        "room" : "work",
        "desc" : "<h3>Studio</h3>Ruang kerjaku. Kalian bisa baca-baca artikel tentang musik/videografi."
    },
    {
        "room" : "children",
        "desc" : "<h3>Kamar Anak</h3>Tempat bermain anak-anakku, Goku, Barak, dan Astu. Teman-teman dan keluarga juga biasa main ke sini."
    },
    {
        "room" : "family",
        "desc" : "<h3>Ruang Keluarga</h3>Di sini tempatku bermain bersama keluarga tercinta, sambil menonton film di home theater."
    },
    {
        "room" : "studio1",
        "desc" : "<h3>Studio Musik</h3>Ruang kreatifku sebagai musisi dan bikin karya-karya lainnya."
    },
    {
        "room" : "studio2",
        "desc" : "<h3>Studio Visual</h3>Ruang kreatifku sebagai filmmaker."
    },
    {
        "room" : "office",
        "desc" : "<h3>Kantor</h3>Ruang kerjaku, buat teman-teman yang ada perlu untuk urusan bisnis, silakan datang."
    },
    {
        "room" : "guest",
        "desc" : "<h3>Kamar Tamu</h3>Banyak tamu yang sering datang di rumahku. Mereka adalah orang-orang yang sangat inspiratif. Nikmati ide dan karya mereka di sini. Tebak siapa yang datang hari ini?"
    },
    {
        "room" : "kitchen",
        "desc" : "<h3>Dapur</h3>Buat yang suka masak, apalagi doyan makan, pasti betah nongkrong di sini."
    },
    {
        "room" : "minibar",
        "desc" : "Yuk, kita nongkrong, bercanda, tertawa, dan gila-gilaan di sini."
    },
    {
        "room" : "pool",
        "desc" : "<h3>Kolam Renang</h3>Buat yang suka main air, tunggu apalagi? Ayo kita nyebur."
    },
    {
        "room" : "garden",
        "desc" : "<h3>Taman</h3>Nikmati pesona dan kekayaan alam Indonesia di taman ini."
    },
    {
        "room" : "garage",
        "desc" : "<h3>Garasi</h3>Hanya untuk kalian yang suka otomotif dan bongkar pasang mesin. Karya-karya kreatif anak bangsa juga banyak loh di sini."
    },
    {
        "room" : "personal",
        "desc" : "<h3>Kamarku</h3> Kamar tidur adalah tempat yang paling privat, tapi khusus di kamar ini, keluarga dan semua teman dekatku boleh kok masuk ke sini."
    },
    {
        "room" : "bukutamu",
        "desc" : "Monggo, di isi bukunya."
    },
    {
        "room" : "lounge",
        "desc" : "Sak udutan karo nonton kolam."
    },
    {
        "room" : "security",
        "desc" : "<h3>Pos Satpam</h3>Bingung atau takut nyasar, tanya Pak Satpam aja."
    },
    {
        "room" : "pendopo",
        "desc" : "<h3>Pendopo</h3>Mari lestarikan budaya bangsa kita sendiri."
    },
    {
        "room" : "gym",
        "desc" : "<h3>Gym</h3>Dibalik tubuh yang sehat, terdapat diet yang ketat. Jangan lupa berolahraga."
    },
    {
        "room" : "amphi",
        "desc" : "<h3>Amphitheater</h3>Di sini teman-teman bisa mengekspresikan diri, yang suka nyanyi, main musik, baca puisi, pamer karya seni, stand up comedy, sulap, atau apa aja, mau numpang curhat juga boleh kok."
    },
    {
        "room" : "river",
        "desc" : "<h3>Sungai</h3>Diving dan laut Indonesia."
    },
    {
        "room" : "secret",
        "desc" : "<b>RixSecret</b>"
    },
    {
        "room" : "cs",
        "desc" : "<i>coming soon</i>"
    }
]

var tv_channel = [
    {
        "channel":"does",
        "episode":"#1",
        "title":"Cara mendapatkan macbook pro"
    },
    {
        "channel":"does",
        "episode":"#2",
        "title":"Berburu mangut lele"
    },
    {
        "channel":"does",
        "episode":"#3",
        "title":"PASSION"
    },
    {
        "channel":"does",
        "episode":"#4",
        "title":"Sekolah alam"
    },
    {
        "channel":"does",
        "episode":"#5",
        "title":"Goku sekolah"
    },
    {
        "channel":"does",
        "episode":"#6",
        "title":"Video untuk Barakallah"
    }
]

// POST TESTIMONI
var d = new Date();

var month = d.getMonth()+1;
var day = d.getDate();

var TODAY_DATE = d.getFullYear() + '/' +
(month<10 ? '0' : '') + month + '/' +
(day<10 ? '0' : '') + day;

var family_member = [
    {   
        'id': 'fam1',
        'pic': 'https://instagram.fcgk4-1.fna.fbcdn.net/vp/06b4ba4d9a41dd8e2bb5740f26e1b074/5AE99FB1/t51.2885-19/s320x320/24126407_1959839621004103_61879758496989184_n.jpg',
        'name': 'Erik Kristianto',
        'nickname': 'Erix Soekamti',
        'birthdate': '31/03/1982',
        'quote': 'Jangan sekali-kali mempercayai kata-kata saya seratus persen, karena ini adalah opini pribadi yang bisa saja salah, bisa juga benar.'
    },
    {
        'id': 'fam2',
        'pic': 'https://instagram.fcgk4-1.fna.fbcdn.net/vp/d7c88ab54f7c0d65067512ec39b36fc9/5B223D37/t51.2885-15/e35/25010737_161106987837065_6222264699631697920_n.jpg',
        'name': 'Herdita Sulistyorini',
        'nickname': 'Mbak Dita',
        'birthdate': '11/04/1979',
        'quote': 'Ada uang abang sayang,<br>tak ada uang ambil tabungan.'
    },
    {
        'id': 'fam3',
        'pic': 'https://instagram.fcgk4-1.fna.fbcdn.net/vp/1e4b7976ba26e95e66b01d83780d4e14/5B226CE6/t51.2885-15/e35/23507852_137876883532400_7783800536557420544_n.jpg',
        'name': 'Godblessyou',
        'nickname': 'Goku',
        'birthdate': '16/04/2004',
        'quote': 'Keep cool, and play minecraft.'
    },
    {
        'id': 'fam4',
        'pic': 'https://instagram.fcgk4-1.fna.fbcdn.net/vp/aa04acb8feed3771d563569895b51a1d/5B05D0EA/t51.2885-15/e35/23498563_931294197019052_6851142800478568448_n.jpg',
        'name': 'Barakallah',
        'nickname': 'Barak',
        'birthdate': '05/05/2014',
        'quote': 'Dont judge a book by its cover.<br>Aku badboy berponi.'
    },
    {
        'id': 'fam5',
        'pic': 'https://instagram.fcgk4-1.fna.fbcdn.net/vp/32ef2237d3dd4bfd3d182f5ebac805eb/5AFFEC9E/t51.2885-15/e35/26863194_361198687677535_8948266668442779648_n.jpg',
        'name': 'Swatiastu',
        'nickname': 'Astu',
        'birthdate': '10/11/2017',
        "quote": '(Breast) Milk for lyfe.'
    }
];

var gallery_images = [
    'amphi.jpeg',
    'office.jpeg',
    'office2.jpeg',
    'soekamtiland1.jpeg',
    'soekamtiland2.jpeg',
    'soekamtiland3.jpeg',
    'soekamtiland4.jpeg',
    'soekamtiland5.jpeg',
    'soekamtiland6.jpeg',
    'soekamtilandmap.jpeg'
]

var event_list = [
    {
        "title": "Gathering Kamtis",
        "about": "Mempererat tali persaudaraan Kamtis Family maupun di luar Kamtis Family bersama Endank Soekamti.",
        "venue": "-",
        "schedule": "3-4 Februari",
        "speakers": "Endank Soekamti",
        "gallery": "-",
        "details": ""
    },
    {
        "title": "Back to Papua",
        "about": "Kedatangannya ke Papua kali ini bukan untuk rekaman lagi, tapi? Penasaran kan?",
        "venue": "Papua",
        "schedule": "6-14 Februari",
        "speakers": "Erix Soekamti",
        "gallery": "-",
        "details": ""
    },
    {
        "title": "Roadshow Salam Indonesia - Homecoming",
        "about": "Roadshow Salam Indonesia kembali ke Yogyakarta!",
        "venue": "-",
        "schedule": "27 Februari",
        "speakers": "Endank Soekamti",
        "gallery": "http://soekamti.com/assets/image/360/0/1516276810_61582111/20180117-roadshow-salam-indonesia-malang.jpg",
        "details": "Masayu (WA): +62 821–3333–4441"
    }
];