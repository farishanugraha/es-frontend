var thisURL = url_wp_es.gym;

// var totalDataContainer = $('#totalpost');
var allDataContainer = $('#alldata');
var titleContainer = $('#title');
var dateContainer = $('#date');
var contentContainer = $('#content');

$(document).ready(function(){
	showData('latest')
	getAllData();
	dataController();
});

function showData(id){
	if(id=='latest'){
		getData(thisURL, function(json){	
			var data = json[0];
			var date = data.date.substr(0, 10);
			titleContainer.html(data.title.rendered);
			dateContainer.html('<i>'+date+'</i>');
			contentContainer.html(data.content.rendered);
		})
	}
	else{
		getData(url_wp_es.posts+id, function(data){
			titleContainer.html(data.title);
			// dateContainer.html('<i>tanggal: '+data.date+'</i>');
			if(data.content==undefined){
				contentContainer.html(data);
			}else{
				titleContainer.html(data.title.rendered);
				contentContainer.html(data.content.rendered);
			}
		})
	}

	getData(thisURL, function(json){
		// totalDataContainer.html('total post: '+json.length);	
	});
}

function getAllData(){
	getData(thisURL, function(json){
			for (var i = 0; i <= json.length - 1; i++) {
				var data = json[i];
				var id = data.id;
				var title = data.title.rendered;
				var excerpt = data.excerpt.rendered;
				var list = '<div class="article"><h4 id="'+id+'">'+title+'</h4></div>';
					allDataContainer.append(list)
			}
		})
}

function dataController(){
	allDataContainer.click(function(e){
		$('#alldata').removeClass('show');
		showData(e.target.id);
		$('#content').scrollTop(0);
	});
}

// gym
$('#article, #close').click(function(e){
	e.stopPropagation();
    $('#maincontent').toggle('fast');
    $('.svg-container').toggleClass('blur');
})

$('#alldata_trigger').click(function(e){
	$('#alldata').toggleClass('show');
})

$('#close').click(function(e){
	$('#alldata').removeClass('show');
})