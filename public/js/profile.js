// CERMIN
if(curUser!=null){
    $.getJSON(url.user+curUser.name,function(json){
        var data = json.result;
        // console.log(data)

        var convregdate = String(data.registered_date);
        var convexpdate = String(data.expired_date);

        var registered_date = convregdate.substr(0,10);
        var expired_date = convexpdate.substr(0,10);

        if(registered_date==expired_date){
            expired_date = 'selamanya';
        }else{
            expired_date = 'tanggal '+expired_date;
        }

        $('#user_name').html(data.name);
        $('#user_role').html(data.role.name);
        $('#user_reg_date').html(registered_date);
        $('#user_exp_date').html(expired_date);
    });
}