$(window).on('load', function(){
    $('#load').fadeOut('slow');
    setTimeout(function(){
        $('#load').remove();
    }, 3000);
});
// CONFIG
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );

// controls config
var controls = new THREE.FirstPersonControls(camera);
controls.lookSpeed = 0.1;
controls.movementSpeed = 10;
controls.noFly = true;
controls.lookVertical = false;
controls.constrainVertical = false;
controls.verticalMin = 1.0;
controls.verticalMax = 1.0;
controls.lon = -100;
controls.lat = 70;

// CAMERA
// =====================================================================
camera.position.set(0, -5, 10);
camera.up = new THREE.Vector3( 0, 0, 0.01 );
camera.lookAt(new THREE.Vector3(0, 0, 0));
document.body.setAttribute('style','overflow:hidden')
// =====================================================================

// RENDERER
// =====================================================================
renderer.setClearColor('#fff');
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );
// =====================================================================

// OBJECT PREVIEW

// CLICK CONTROL
// =====================================================================
raycaster = new THREE.Raycaster();
mouse = new THREE.Vector2();
document.addEventListener( 'mousedown', onDocumentMouseDown, false );
function onDocumentMouseDown(e){

	mouse.x = ( e.clientX / renderer.domElement.clientWidth ) * 2 - 1;
	mouse.y = - ( e.clientY / renderer.domElement.clientHeight ) * 2 + 1;

	raycaster.setFromCamera( mouse, camera );

	var intersects = raycaster.intersectObjects( objects );

	if(intersects[0]!=undefined){
		var name = intersects[0].object.name;
		// console.log(name)

		if (name=='tv'){
			var tv = intersects[0].object;
			var ySpeed=1;
			var interval = setInterval(function(){
				if(camera.position.y>=tv.position.y-10){
					// console.log('STOP')
					window.location = '/does';
					clearInterval(interval)
				}else{
					camera.position.y += ySpeed;
				}

				if(camera.position.x < tv.position.x){
					camera.position.x += ySpeed/2;
				} else if (camera.position.x == tv.position.x){
					camera.position.x = tv.position.x
				}
				else{
					camera.position.x = 0;
				}
				if(camera.position.z <= tv.position.z){
					camera.position.z += ySpeed;
				}else{

				}
			},1000/100);
		}
		else if (name == ''){
			console.log('ga ada namanya')
			$('.object').hide();
		}
		else if (name == 'door_R'){
			window.location = '/security'
		}
		else if (name == 'door_L'){
			window.location = '/studio/music'
		}
		else if (name == 'album'){
			var album = intersects[0].object;
			var ySpeed=0.25;
			var interval = setInterval(function(){
				if(camera.position.y>=album.position.y-7.5){
					// console.log('STOP')
					window.location = '/main/gallery';
					clearInterval(interval)
					
				}else{
					camera.position.y += ySpeed*1.1;
				}
				if(camera.position.x > album.position.x){
					camera.position.x -= ySpeed/3;
				} else if (camera.position.x == album.position.x){
					camera.position.x = album.position.x
				}
				else{
					camera.position.x = 0;
				}

				camera.position.z -= ySpeed/2.5;

				if(album.rotation.z >= 0){
					album.rotation.z -= ySpeed;
				}else{}
				if(album.rotation.y <= 0.1){
					album.rotation.y += ySpeed;
				}else{}
				if(album.rotation.x >= -4.71){
					album.rotation.x -= ySpeed;
				}else{}
				if(album.position.z <= 5){
					album.position.z += ySpeed;
				}else{}

			},1000/100);
		}
		else if (name == 'newspaper'){
			var newspaper = intersects[0].object;
			var ySpeed=0.25;
			var interval = setInterval(function(){
				if(camera.position.y>=newspaper.position.y-7.5){
					// console.log('STOP')
					window.location = '/main/news';
					clearInterval(interval)
					
				}else{
					camera.position.y += ySpeed*1.3;
				}
				if(camera.position.x < newspaper.position.x){
					camera.position.x += ySpeed/2.5;
				} else if (camera.position.x == newspaper.position.x){
					camera.position.x = newspaper.position.x
				}
				else{
					camera.position.x = 0;
				}

				camera.position.z -= ySpeed/2;

				if(newspaper.rotation.z >= 0){
					newspaper.rotation.z -= ySpeed;
				}else{}
				if(newspaper.rotation.y >= -0.1){
					newspaper.rotation.y -= ySpeed;
				}else{}
				if(newspaper.rotation.x >= -4.71){
					newspaper.rotation.x -= ySpeed;
				}else{}
				if(newspaper.position.z <= 5){
					newspaper.position.z += ySpeed;
				}else{}

			},1000/100);
		}
		else if (name == 'glass'){
			alertCustom('loading', 'Tes Bakat <br/> <i>Mohon maaf, halaman tes bakat belum tersedia.</i>', 3000);
		}
		else if (name == 'pooltable'){
			alertCustom('loading', 'Meja Billiard <br/> <i>Mohon maaf, belum tersedia.</i>', 3000);
		}
		else if (name == 'mirror'){
		}
		else if (name == 'calendar'){
			var calendar = intersects[0].object;
			var ySpeed=1;
			var interval = setInterval(function(){
				if(camera.position.y>=calendar.position.y-10){
					// console.log('STOP')
					window.location = '/main/event';
					clearInterval(interval)
				}else{
					camera.position.y += ySpeed;
				}

				if(camera.position.x > calendar.position.x){
					camera.position.x -= ySpeed/2;
				} else if (camera.position.x == calendar.position.x){
					camera.position.x = calendar.position.x
				}
				else{
					camera.position.x = 0;
				}
				if(camera.position.z <= calendar.position.z){
					camera.position.z += ySpeed;
				}else{

				}
			},1000/100);
		}
		else if (name == 'guestbook'){

			var gb = intersects[0].object;
			var ySpeed=0.25;
			var interval = setInterval(function(){
				if(camera.position.y>=gb.position.y-5.5){
					window.location = '/main/editor';
					clearInterval(interval)
					
				}else{
					camera.position.y += ySpeed*1.2;
				}
				if(camera.position.x > gb.position.x){
					camera.position.x -= ySpeed/2;
				} else if (camera.position.x == gb.position.x){
					camera.position.x = gb.position.x
				}
				else{
					camera.position.x = 0;
				}

				camera.position.z -= ySpeed/1.7;

				if(gb.rotation.z >= 0){
					gb.rotation.z -= ySpeed;
				}else{}
				if(gb.rotation.y <= 0.1){
					gb.rotation.y += ySpeed;
				}else{}
				if(gb.rotation.x >= -4.71){
					gb.rotation.x -= ySpeed;
				}else{}
				if(gb.position.z <= 5){
					gb.position.z += ySpeed;
				}else{}

			},1000/100);
		}
		else if (name == 'remote'){
			// console.log(objects[0])
			// objects[0].material.color.r = Math.random()*1.5;
			// objects[0].material.color.g = Math.random()*1.5;
			// objects[0].material.color.b = Math.random()*1.5;
			window.location = '/does';
		}
		else {
			alertCustom('error','Anda belum bisa mengakses ruangan ini. Silahkan <b>upgrade</b> membership dulu.',3000);
			$('.object').hide();
		}


	}else{
		$('.object').hide();
		var interval = setInterval(function(){
			// console.log(camera.position.y)
			
			// camera.position.set(0,0,10);

			// if(camera.position.y>= -4.8){
			// 	camera.position.y -= 1.8;
			// }else{
			// 	camera.position.y = -4.8;
			// }
			// if(camera.position.x > 0){
			// 	camera.position.x -= 1;
			// }else{
			// 	camera.position.x = 0;
			// }
			// if(camera.position.z > 10){
			// 	camera.position.z -= 0.3;
			// }else{
			// 	camera.position.z = 10;
			// 	// console.log('STOP')
			// 	clearInterval(interval)
			// }
		},1000/60);
	}
}
document.addEventListener( 'mousemove', onDocumentMouseMove, false );
function onDocumentMouseMove(e){
	mouse.x = ( e.clientX / renderer.domElement.clientWidth ) * 2 - 1;
	mouse.y = - ( e.clientY / renderer.domElement.clientHeight ) * 2 + 1;

	raycaster.setFromCamera( mouse, camera );

	var intersects = raycaster.intersectObjects( objects );

	if(intersects[0]!=undefined){
		var name = intersects[0].object.name;
		if (name){
			objectPreview(name);
		}else{
			$('.object').hide();
			$('#flashInfo').hide();
		}
	}else{
		$('#flashInfo').hide();
	}
}
// =====================================================================
var object_desc = {
	"tv": "<h3>TV</h3>Video spesial dan DOES episode terbaru.",
	"remote": "<h3>Remote</h3>",
	"newspaper": "<h3>Koran</h3>Berita terbaru!",
	"guestbook": "<h3>Buku Tamu</h3>Kirim pesan, kritik, dan saran.",
	"album": "<h3>Album Foto</h3>Koleksi foto dan gambar Soekamtiland.",
	"calendar": "<h3>Kalender</h3>Jadwal acara bulan ini.",
	"mirror": "<h3>Cermin</h3>Siapa kamu?",
	"door_L": "<h3>ke Studio Musik</h3>",
	"door_R": "<h3>ke Pos Satpam</h3>" 
}
function objectPreview(obj){
	$('#flashInfo').html(object_desc[obj]);
	$('#flashInfo').show('fast');
}

// object
var objects = [];
// planeInit();
texturedPlaneInit();
lightsInit();
tvInit();
tableInit();
buffetInit();
// pooltableInit();
doorInit();
sofaInit();
albumInit();
newspaperInit();
// drinkInit();
// mirrorInit();
// remoteInit();
guestbookInit();
calendarInit();


function planeInit(){
	var plane = getPlane(800, '#fff');
	var wall = getPlane(100,'#fff');
	var wall_L = getPlane(100,'#fff');
	var wall_R = getPlane(100,'#fff');
	wall.position.y = 40;
	wall.position.z = 45;
	wall.rotation.x = 4.71;

	wall_R.position.x = 25;
	wall_R.position.z = 45;
	wall_R.rotation.y = 4.710;

	wall_L.position.x = -25;
	wall_L.position.z = 45;
	wall_L.rotation.y = -4.710;

	scene.add(wall)
	scene.add(wall_R)
	scene.add(wall_L)
	scene.add(plane)
}

function texturedPlaneInit(){
	tx_loader.load('/objects/textures/wall.jpg', function (texture){
		var material = new THREE.MeshBasicMaterial({map: texture})

		var woodwall = getBoxTextured(50,50,1.5, material);
		woodwall.position.y = 40.6;
		woodwall.position.z = 25;
		woodwall.rotation.x = Math.PI/2;
		woodwall.castShadow = true;
		scene.add( woodwall );
		
		},
		function (xhr) {console.log( (xhr.loaded / xhr.total * 100) + '% loaded' )},
		function ( xhr ) {console.log( 'An error happened' );}
	);
	tx_loader.load('/objects/textures/floor.jpg', function (texture){
		var material = new THREE.MeshBasicMaterial({map: texture})

		var floor = getBoxTextured(50,50,1.5, material);
		floor.position.y = 20.5;
		floor.position.z = 0;
		floor.rotation.x = 0;
		floor.castShadow = true;
		scene.add( floor );
		
		},
		function (xhr) {console.log( (xhr.loaded / xhr.total * 100) + '% loaded' )},
		function ( xhr ) {console.log( 'An error happened' );}
	);

	tx_loader.load('/objects/textures/wall1.jpg', function (texture){
		var material = new THREE.MeshBasicMaterial({map: texture})

		var wall_L = getBoxTextured(50,50,1.5, material);
		wall_L.position.y = 15;
		wall_L.position.z = 25;
		wall_L.position.x = -25.5;
		wall_L.rotation.y = Math.PI/2;
		wall_L.castShadow = true;
		scene.add( wall_L );
		
		},
		function (xhr) {console.log( (xhr.loaded / xhr.total * 100) + '% loaded' )},
		function ( xhr ) {console.log( 'An error happened' );}
	);

	tx_loader.load('/objects/textures/wall1.jpg', function (texture){
		var material = new THREE.MeshBasicMaterial({map: texture})

		var wall_R = getBoxTextured(50,50,1.5, material);
		wall_R.position.y = 15;
		wall_R.position.z = 25;
		wall_R.position.x = 25.5;
		wall_R.rotation.y = Math.PI/2;
		wall_R.castShadow = true;
		scene.add( wall_R );
		
		},
		function (xhr) {console.log( (xhr.loaded / xhr.total * 100) + '% loaded' )},
		function ( xhr ) {console.log( 'An error happened' );}
	);
}

function lightsInit(){
	var sphere = getSphere(0.2);
	var pointLight = getPointLight(1);
	pointLight.add(sphere);
	pointLight.position.z = 35;
	pointLight.position.y = 10;

	var boundingBoxHelperObject = new THREE.BoxHelper( pointLight )
	scene.add(pointLight);
	scene.add(boundingBoxHelperObject)
}

function tvInit(){

	tx_loader.load('/objects/textures/tv.png', function (texture){
		var material = new THREE.MeshBasicMaterial({map: texture})

		var tv = getBoxTextured(15,1.5,8, material);
		tv.position.y = 40;
		tv.position.z = 15;
		tv.rotation.x = 0;
		tv.castShadow = true;
		tv.material.shininess = 100;
		tv.name = 'tv';
		objects.push(tv)
		scene.add( tv );
		
		},
		function (xhr) {console.log( (xhr.loaded / xhr.total * 100) + '% loaded' )},
		function ( xhr ) {console.log( 'An error happened' );}
	);

	// var tv = getBox(15,1.5,8);
	// tv.position.z = 15;
	// tv.position.y = 40;
	// tv.position.x = 0;
	// tv.material.shininess = 100;
	// tv.name = 'tv';
	// objects.push(tv)
	// scene.add( tv );
}

function calendarInit(){
	tx_loader.load('/objects/textures/calendar.jpg', function (texture){
		var material = new THREE.MeshBasicMaterial({map: texture})

		var calendar = getBoxTextured(5,0.5,7, material);
		calendar.position.z = 13;
		calendar.position.y = 40;
		calendar.position.x = -17;
		calendar.material.shininess = 100;
		calendar.name = 'calendar';
		objects.push(calendar)
		scene.add( calendar );
		
		},
		function (xhr) {console.log( (xhr.loaded / xhr.total * 100) + '% loaded' )},
		function (xhr) {console.log( 'An error happened' );}
	);
}

function tableInit(){
	tx_loader.load('/objects/textures/wood2.jpg', function (texture){
		var material = new THREE.MeshBasicMaterial({map: texture})

		var table = getBoxTextured(18,8,3, material);
		table.position.z = 2;
		table.position.y = 13;
		table.castShadow = true;
		objects.push(table)
		scene.add( table );
		
		},
		function (xhr) {console.log( (xhr.loaded / xhr.total * 100) + '% loaded' )},
		function (xhr) {console.log( 'An error happened' );}
	);
}

function buffetInit(){
	tx_loader.load('/objects/textures/buffet.jpg', function (texture){
		var material = new THREE.MeshBasicMaterial({map: texture})

		var buffet = getBoxTextured(8,1.5,20, material);
		buffet.position.x = 18;
		buffet.position.z = 10;
		buffet.position.y = 38;
		buffet.castShadow = true;
		objects.push(buffet)
		scene.add( buffet );
		
		},
		function (xhr) {console.log( (xhr.loaded / xhr.total * 100) + '% loaded' )},
		function (xhr) {console.log( 'An error happened' );}
	);
}

function pooltableInit(){
	tx_loader.load('/objects/textures/pooltable.png', function (texture){
		// var material = new THREE.MeshBasicMaterial({map: texture})
		var material = new THREE.MeshPhongMaterial({color:'grey'})

		var pooltable = getBoxTextured(18,8,1.5, material);
		pooltable.position.z = 5;
		// pooltable.rotation.z = Math.PI/2;
		pooltable.position.x = -5;
		pooltable.position.y = 30;
		pooltable.castShadow = true;
		pooltable.name = 'pooltable';
		objects.push(pooltable)
		scene.add( pooltable );
		
		},
		function (xhr) {console.log( (xhr.loaded / xhr.total * 100) + '% loaded' )},
		function (xhr) {console.log( 'An error happened' );}
	);
}

function albumInit(){
	tx_loader.load('/objects/textures/album.jpg', function (texture){
		var material = new THREE.MeshBasicMaterial({map: texture})

		var album = getBoxTextured(3,3,0.3, material);
		album.position.z = 3.7;
		album.position.y = 14;
		album.position.x = -5;
		album.name = 'album';
		scene.add(album);
		objects.push(album);
		},
		function (xhr) {console.log( (xhr.loaded / xhr.total * 100) + '% loaded' )},
		function (xhr) {console.log( 'An error happened' );}
	);
}

function guestbookInit(){
	tx_loader.load('/objects/textures/paper.jpg', function (texture){
		var material = new THREE.MeshBasicMaterial({map: texture})

		var guestbook = getBoxTextured(5,2,0.3, material);
		guestbook.position.z = 3.7;
		guestbook.position.y = 10;
		guestbook.position.x = -5;
		guestbook.name = 'guestbook';
		scene.add(guestbook);
		objects.push(guestbook);
		},
		function (xhr) {console.log( (xhr.loaded / xhr.total * 100) + '% loaded' )},
		function (xhr) {console.log( 'An error happened' );}
	);
}

function newspaperInit(){
	tx_loader.load('/objects/textures/newspaper.jpg', function (texture){
		var material = new THREE.MeshBasicMaterial({map: texture})

		var newspaper = getBoxTextured(4,2,0.1, material);
		newspaper.position.z = 3.7;
		newspaper.rotation.z = 1;
		newspaper.position.y = 12;
		newspaper.position.x = 5;
		newspaper.name = 'newspaper';
		scene.add(newspaper);
		objects.push(newspaper);
		},
		function (xhr) {console.log( (xhr.loaded / xhr.total * 100) + '% loaded' )},
		function (xhr) {console.log( 'An error happened' );}
	);
}

function doorInit(){
	// var door_R = getBox(1.5, 8, 30);
	// door_R.position.x = 25;
	// door_R.position.y = 20;
	// door_R.position.z = 2;
	// door_R.name = 'door_R';
	// objects.push(door_R)
	// scene.add(door_R);

	// var door_L = getBox(1.5, 8, 30);
	// door_L.position.x = -25;
	// door_L.position.y = 20;
	// door_L.position.z = 2;
	// door_L.name = 'door_L';
	// objects.push(door_L)
	// scene.add(door_L);

	tx_loader.load('/objects/textures/door.png', function (texture){
		var material = new THREE.MeshBasicMaterial({map: texture})

		var door_R = getBoxTextured(1, 8, 21, material);
		door_R.position.x = 25;
		door_R.position.y = 20;
		door_R.position.z = 10;
		door_R.name = 'door_R';
		objects.push(door_R)
		scene.add(door_R);

		var door_L = getBoxTextured(1, 8, 21, material);
		door_L.position.x = -25;
		door_L.position.y = 20;
		door_L.position.z = 10;
		door_L.rotation.x = (Math.PI/2)*2;
		door_L.name = 'door_L';
		objects.push(door_L)
		scene.add(door_L);
		
		},
		function (xhr) {console.log( (xhr.loaded / xhr.total * 100) + '% loaded' )},
		function (xhr) {console.log( 'An error happened' );}
	);
}

function sofaInit(){
	manager.onProgress = function (item, loaded, total) {console.log( item, loaded, total);}
	fbxloader.load('/objects/sofa/sofa-1.fbx',
		function(object){
			$('#loader').fadeOut();
			object.position.set(-16, 6, 1)
			object.scale.set(7,7,7)
			object.rotation.set(1.5,Math.PI/2,0)
			object.castShadow = true;
			// console.log(object.castShadow)
			scene.add(object);
		}, 
		// on progress
		function (xhr) {
			$('#loader').show();
			if (xhr.lengthComputable) {
				var percentComplete = xhr.loaded/xhr.total * 100;
				$('#load_progress').html(Math.round(percentComplete, 2) + '%');
				console.log(Math.round(percentComplete, 2) + '% downloaded');
			}
		}, 
		// on error
		function(xhr){
			console.error(xhr)
		}
	);
}

function drinkInit(){
	tx_loader.load('/objects/textures/beer.jpg', function (texture){
		var material = new THREE.MeshBasicMaterial({map: texture})

		var glass = getCylinder(0.5,0.5,1,20, material);
		glass.position.z = 4;
		glass.position.y = 11;
		glass.position.x = 0.1;
		glass.rotation.x = 1.55;
		glass.name = 'glass';
		scene.add(glass);
		objects.push(glass);
		},
		function (xhr) {console.log( (xhr.loaded / xhr.total * 100) + '% loaded' )},
		function (xhr) {console.log( 'An error happened' );}
	);
}

function mirrorInit(){
	var mirror = getCircle(2);
	mirror.position.x = -24.5;
	mirror.position.y = 27;
	mirror.position.z = 13;
	mirror.material.shininess = 1000;
	mirror.rotation.y = 1.5;

	mirror.name = 'mirror';
	objects.push(mirror);
	scene.add(mirror);
}

function remoteInit(){
	var remote = getBox(0.5,1,0.3);
	remote.position.x = 6;
	remote.position.y = 10;
	remote.position.z = 3.5;
	remote.name = 'remote';
	objects.push(remote);
	scene.add(remote);
}

// fix functions
function getPlane(size, color){
	var geometry = new THREE.PlaneGeometry(size, size);
	var material = new THREE.MeshPhongMaterial({
		color: color,
		side: THREE.DoubleSide,
		shininess: 0.1
	});
	var mesh = new THREE.Mesh(
		geometry,
		material
	);
	mesh.receiveShadow = true;

	return mesh;
}

function getPointLight(intensity) {
	var light = new THREE.PointLight(0xffffff, intensity);
	light.castShadow = true;

	return light;
}

function getBoxTextured(w, h, d, material){
	var geometry = new THREE.BoxGeometry(w,h,d);
	var material = material;
	var mesh = new THREE.Mesh(
		geometry,
		material
	);
	mesh.castShadow = true;
	// mesh.position.y = mesh.geometry.parameters.height/2;

	return mesh;
}

function getBox(w, h, d){
	var geometry = new THREE.BoxGeometry(w,h,d);
	var material = new THREE.MeshPhongMaterial({
		color: 'rgb(120, 120, 120)'
	});
	var mesh = new THREE.Mesh(
		geometry,
		material
	);
	mesh.castShadow = true;
	// mesh.position.y = mesh.geometry.parameters.height/2;

	return mesh;
}

function getSphere(size){
	var geometry = new THREE.SphereGeometry(size, 24, 24);
	var material = new THREE.MeshBasicMaterial({
		color: 'rgb(255, 255, 255)'
	});
	var mesh = new THREE.Mesh(
		geometry,
		material
	);

	mesh.shininess = 2000;

	return mesh;
}

function getCylinder(rt, rb, h, rs, material){
	var geometry = new THREE.CylinderGeometry( rt, rb, h, rs );
	var material = material;
	var cylinder = new THREE.Mesh( geometry, material );
	cylinder.castShadow = true;
	return cylinder;
}

function getCircle(r){
	var geometry = new THREE.CircleGeometry( r, 32 );
	var material = new THREE.MeshPhongMaterial( { color: 0xffffff } );
	var circle = new THREE.Mesh( geometry, material );

	return circle;
}



// render
var animate = function () {
	var delta = clock.getDelta();
	requestAnimationFrame( animate );

	renderer.render(scene, camera);
	controls.update(delta);
};animate();

//==========================================