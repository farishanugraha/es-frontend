// GET TOKEN && SET USER
function getToken(){
    token = localStorage.getItem('uToken')
    // console.log(token);
    // if else utoken is valid
    if (token == null) {
        roomAccess('main');
        roomAccess('register');
        roomAccess('login');
        roomAccess('security');
        roomAccess('invite');
    } 
    else {
        $.ajax({
            type: "GET",
            url: url.token,
            headers: {'token': token},
            success: function(data) {
                var dataResult = JSON.stringify(data.result);
                localStorage.setItem('curUser',dataResult);
            },
            error: function(err){
                //result = JSON.parse(err.responseText);
                //alert('error'+result.message);
                console.log(err)
            }
        });
    }               
}
// CHECK AUTH
function checkAuth(){
    isAuthed = localStorage.getItem('isAuthed');

    //NO AUTH. BERARTI SETAN
    if (isAuthed!="true" || isAuthed == null) {
        console.log('%cNOT LOGGED IN', 'background:maroon;color:white');
        console.log('%cUser: SETAN'+
                '\nAccess: SETAN',
                'background:black;color:white');
        
        //give permission as visitor
        givePermission('setan');
        $('.cloud').show();
        $('#cloud2').show();
        $('#logout').hide();
    }

    //SUCCESS. SELEKSI ROLE
    else{
        // var menu = 
        // '<li class="demo-btn" id="logOut"><a href="#"><img src="/img/logout.svg"></a></li>';
        $('#mainMenu').remove();
        $('#mainMenu').removeClass('center');
        if (window.location.pathname == '/'){
            $('#logOut').show();
        }

        $('#mainSecret').show();
        $('.cloud').remove();
        $('#cloud1').remove();
        $('#arrow').remove();
        $('#logout').show();


        //get id id-an
        var upgradecode;
        if(curUser==null){
            localStorage.removeItem('curUser');
            localStorage.removeItem('isAuthed');
            localStorage.removeItem('uToken');
            window.location = '/';
            upgradecode = window.location.pathname;
        }else {upgradecode = curUser.uid}

        $.getJSON(url.user+curUser.name, function(json){
            var data = json.result;

            if(data.name=="yoyoyo"){
                data.role.id = 2;
            }

            // TAMU
            if(data.role.id == 1){
                console.log('%cLogged In: '+isAuthed+
                    '\nUser: '+data.name+
                    '\nAccess: '+data.role.name,
                    'background:green;color:white');
                givePermission('free');
            }

            // TEMAN
            else if(data.role.id == 2){
                $('#cloud').remove();
                $('#cloud2').remove();
                console.log('%cLogged In: '+isAuthed+
                    '\nUser: '+data.name+
                    '\nAccess: '+data.role.name,
                    'background:darkslateblue;color:gold');
                givePermission('premium');
            }

            // SOULMATE
            else if(data.role.id == 3){
                console.log('%cLogged In: '+isAuthed+
                    '\nUser: '+data.name+
                    '\nAccess: '+data.role.name,
                    'background:gold;color:black');
                givePermission('soulmate');
            }

            // FAMILY
            else if(data.role.id == 4){
                console.log('%cLogged In: '+isAuthed+
                    '\nUser: '+data.name+
                    '\nAccess: '+data.role.name,
                    'background:gold;color:red');
                givePermission('family');
            }
        });
    }
}

function givePermission(role){
    roomPreview();

    if(role=='setan'){
        //get id id-an
        var upgradecode;
        if(curUser==null){
            upgradecode = window.location.pathname;
        }else{
            upgradecode = curUser.uid
        }

        if(window.location.pathname == '/register'){
            // console.log('register')
        } 
        // else if(window.location.pathname == '/main'){
        //     // console.log('main')
        // } 
        else if(window.location.pathname == '/upgrade/'+upgradecode){
            // console.log('upgrade')
        } 
        else if (window.location.pathname =='/login'){
            // console.log('login')
        }
        else if (window.location.pathname =='/'){
            // console.log('home')
        }
        else if (window.location.pathname =='/forgot_pass'){
            // console.log('fp')
        }
        else if (window.location.pathname.substr(0,16)=='/reset-password/'){
            // console.log('fp')
        }
        else if (window.location.pathname =='/security'){
            // console.log('security')
        }
        else if (window.location.pathname =='/invite'){
            // console.log('security')
        }
        else{
            window.location = '/login';
        }

        $('.room-trigger').click(function(){
            if($(this).attr('id')!='main'){
                alertCustom('loading', 
                    '<b>SATPAM: </b>Kamu belum bisa mengakses halaman ini. silahkan <b>login</b> dulu.',0)
                setTimeout(function(){
                    window.location = '/login';
                },3000)
            }else{
                window.location = '/main';
            }
        });
    }
    else if(role=='free'){
        $('#cloud2').show();
        //give page for free member
        roomAccess('security');
        roomAccess('main');
        roomAccess('register');
        roomAccess('login');
        // roomAccess('confirm');
        roomAccess('schedule');
        roomAccess('does');

        if(window.location.pathname == '/register'){
            window.location = '/main';
        } 
        else if(window.location.pathname == '/main'){
            // console.log('main')
            $('#freeMenu').show();
        } 
        else if(window.location.pathname == '/upgrade/'+curUser.uid){
            // console.log('upgrade')
        } 
        else if (window.location.pathname =='/login'){
            window.location = '/main';
        }
        else if (window.location.pathname =='/'){
            // console.log('home')
        }
        else if (window.location.pathname =='/forgot_pass'){
            // console.log('fp')
            window.location = '/upgrade';
        }
        else if (window.location.pathname =='/does'){
            // console.log('does')
        }
        else if (window.location.pathname =='/schedule'){
            // console.log('schedule')
        }
        else if (window.location.pathname =='/event'){
            // console.log('schedule')
        }
        else if (window.location.pathname =='/security'){
            // console.log('security')
        }
        else if (window.location.pathname =='/main/guestbook'){
            // console.log('security')
        }
        else if (window.location.pathname =='/main/gallery'){
            
        }
        else if (window.location.pathname =='/main/news'){
            // console.log('security')
        }
        else if (window.location.pathname =='/main/event'){
            // console.log('security')
        }
        else if (window.location.pathname =='/profile'){
            // console.log('security')
        }
        else if (window.location.pathname =='/dummy'){
            // console.log('security')
        }
        else if (window.location.pathname =='/invite'){
            // console.log('security')
        }
        else{
            window.location = '/upgrade/'+curUser.uid;
        }

        $('.room-trigger').click(function(){
            var thisid = $(this).attr('id');
            if(thisid == 'main'){
                window.location = '/main';
            }
            else if(thisid == 'security'){
                window.location = '/security'; 
            }
            else{
                alertCustom('loading','SATPAM: Ruangan ini hanya untuk teman, soulmate, dan family.<br> Silahkan <a href="/upgrade/'
                    +curUser.uid+'">upgrade</a> membership dulu, atau nikmati fitur yang tersedia di <b><a href="/main">ruang tamu</a>.</b>',0);
                // setTimeout(function(){
                //     window.location = '/upgrade/'+curUser.uid;
                // },3000);
            }
        });
    }
    else if(role=='premium'){
        allAccess();
        if(window.location.pathname == '/register'){
            window.location = '/main';
        }
        else if (window.location.pathname =='/login'){
            window.location = '/main';
        }
        else if (window.location.pathname =='/invite'){
            // console.log('security')
        }
    }
    else if(role=='soulmate'){
        console.log('kamu soulmatenya mas erix lho')
        allAccess();
    }
    else if(role=='family'){
        console.log('LONG LIVE MY FAMILY')
        allAccess();
        roomAccess('secret');
    }
    else{
        console.log('who are you?')
    }
}

function trigger_animate(posisi){
    // console.log('POSISI: ', posisi)
    arr_posisi = posisi.split(" ");
     var angka = parseInt(arr_posisi[0]);
     angka-=10;
     angka = angka.toString();
     arr_posisi[0] = angka;
     return arr_posisi.toString();
}

function roomPreview(){
    var string;
    var semula;
    $( '.room-trigger' ).hover( function(e) {
        var hoveredRoom = this.id;

        // UNDER CONSTRUCTION
        //======================================
        // string = "#"+hoveredRoom;
        // semula = $(string).attr('d');
        // arr_string = $(string).attr('d').split(",");

        // angkabaru = trigger_animate(arr_string[1]);

        // arr_string[1] = angkabaru;
        // arr_string=arr_string.toString();

        // $(string).attr('d', arr_string);3

        //======================================

        var data = rooms_preview;
        for (var i = 0; i < data.length; i++) {

            if(hoveredRoom!=data[i].room){
                $('#flashInfo').html(data[data.length-1].desc)
            }else{
                $('#flashInfo').html(data[i].desc);
                break;
            }
        }

        $('#flashInfo').show('fast');

    }, function() {
        // console.log(string, semula)
        $('#flashInfo').hide();
        // $(string).attr('d', semula);
    });

    $(document).on("mousemove",function(e) {  
        var ax = -($(window).innerWidth()/2- e.pageX)/20;
        var ay = ($(window).innerHeight()/2- e.pageY)/10;

        if (window.location.pathname == '/'){
            if (e.pageY > ($(window).innerHeight()-100) ){
                $('#security').attr('style','fill:red; opacity:0.3');
            }else{
                $('#security').attr('style','fill:#c9c9c9;stroke:#000000;stroke-width:0.30000001;opacity:0.1');
            }
        }
        
        var x = e.clientX;
        var y = e.clientY;
        if(e.pageX < 150 ){
            $('#flashInfo').css( { top: y-100, left: x+80, position: 'absolute' } );
        }else{
            $('#flashInfo').css( { top: y-100, left: x-200, position: 'absolute' } );
        }

        // if(y > 650){
        //     $('#flashInfo').css( { top: y-125, left: x-225, position: 'absolute' } );
        // }
    });

    $('#logout').mouseover(function(){
        // $('#flashInfo').html('logout'); 
    });

    $('#guest_to_map').hover(function(e){
        $('#flashInfo').html('Kembali ke peta');
        $('#flashInfo').show();
    }, function(e){
        $('#flashInfo').hide();
    })
}

function roomAccess(page){
    if(page=='studio1' || page=='studio2'){
        if(page=='studio1'){
            $('#'+page).click(function(){
                window.location = '/studio/music';
            });
        }else if(page=='studio2'){
            $('#'+page).click(function(){
                window.location = '/studio/video';
            });
        }else{}
    }else{
        $('#'+page).click(function(){
            window.location = '/'+page;
        });
    }
}

function roomControl(){
    
}

function allAccess(){
    $('#freeMenu').show();
    $('#cloud2').remove();

    roomAccess('main');
    roomAccess('register');
    roomAccess('login');
    roomAccess('children');
    roomAccess('family');
    roomAccess('office');
    roomAccess('studio1');
    roomAccess('studio2');
    roomAccess('guest');
    roomAccess('personal');
    roomAccess('kitchen');
    roomAccess('garden');
    roomAccess('pool');
    roomAccess('garage');

    roomAccess('event');
    roomAccess('schedule');
    roomAccess('does');
    roomAccess('security');

    roomAccess('profile');
    roomAccess('minibar');
    roomAccess('security');
    roomAccess('gym');
    roomAccess('amphi');
    roomAccess('pendopo');
    roomAccess('lounge');

    $('#river').click(function(){
        window.location = '/pool';
    });
}

function logout(){
    //logout
    $('#logout').click(function(){
        // var c = confirm("Yakin mau pamit?");
        // if (c == true){
        //     localStorage.removeItem('curUser');
        //     localStorage.removeItem('isAuthed');
        //     localStorage.removeItem('uToken');
        //     window.location = '/';
        // } else {
        //     console.log('user pressed cancel')
        // }

        localStorage.removeItem('curUser');
        localStorage.removeItem('isAuthed');
        localStorage.removeItem('uToken');
        window.location = '/';
    });
}

function alertCustom(type, messages, timeout){
    // console.log('alert custom triggered');
    $('#flashInfo').hide();    
    if(type=='success'){
        $('#flashMessage').attr('style',
        'background-color:#dff0d8;color:green;');
    }else if(type=='error'){
        $('#flashMessage').attr('style',
        'background-color:#f1dddd;color:maroon;');
    }else if(type=='loading'){
        $('#flashMessage').attr('style',
        'background-color:#fcf8e3;color:brown;');
    }else if(type=='choice'){
        console.log('coming soon')
    }
    else{
        console.log('type not defined')
    }

    $('#flashMessage').html(messages);
    $('#flashMessage').show('fast');

    if(timeout!=0){
        setTimeout(function(){
            $('#flashMessage').hide('fast');
        }, timeout);
    }else{
        // console.log('alert always show until redirect')
    }
}

function goBack(){window.history.back();}

function getData(url, callback){
    $.getJSON(url, function(json){
        return callback(json);
    })
}

function getContent(categories, callback){
    $.getJSON(url_wp_es[categories], function(json){
        return callback(json);
    })
}

function browserDetection() {
    //Check if browser is IE
    if (navigator.userAgent.search("MSIE") >= 0) {
        // insert conditional IE code here
        browserAlert();
    }
    //Check if browser is Chrome
    else if (navigator.userAgent.search("Chrome") >= 0) {
        // insert conditional Chrome code here
    }
    //Check if browser is Firefox 
    else if (navigator.userAgent.search("Firefox") >= 0) {
        // insert conditional Firefox Code here
        browserAlert();
    }
    //Check if browser is Safari
    else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
        // insert conditional Safari code here
        browserAlert();
    }
    //Check if browser is Opera
    else if (navigator.userAgent.search("Opera") >= 0) {
        // insert conditional Opera code here
        browserAlert();
    }
}

function browserAlert(){
    if(window.location.pathname == '/' && !curUser){
        $('.browser-alert').show('fast');
    }else{
        $('.browser-alert').hide();
    }
    $('#closeAlert').click(function(e){
        $('.browser-alert').hide('fast');
    })
}

function scaling(trig){
    var map = $('.map').css('transform');

    var values = map.split('(')[1];
    values = values.split(')')[0];
    values = values.split(',');

    var a = values[0];
    var b = values[1];

    var scale = Math.sqrt(a*a + b*b);
    // console.log(scale) // .80

    var curVal = scale;
    // console.log(curVal)

    if(curVal<1){
        $('svg').css('width', '100%');
    }else{
        $('svg').css('width', 'initial');
    }

    if(trig=='out' && curVal>0.8){
        curVal -= 0.1;
        $('.map').css('transform', 'scale('+curVal+')')
    }else if(trig=='in' && curVal<1.3){
        curVal += 0.1;
        $('.map').css('transform', 'scale('+curVal+')')
    }else{
        // console.log('trigger not defined')
    }
}

function newJsonParse(){
    // fungsi baru untuk bikin json parse
    // console.log('%c json-parser ready','color:green');
    (function($){
        $.fn.serializeObject = function(){
            var self = this,
                json = {},
                push_counters = {},
                patterns = {
                    "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                    "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                    "push":     /^$/,
                    "fixed":    /^\d+$/,
                    "named":    /^[a-zA-Z0-9_]+$/
                };
            this.build = function(base, key, value){
                base[key] = value;
                return base;
            };
            this.push_counter = function(key){
                if(push_counters[key] === undefined){
                    push_counters[key] = 0;
                }
                return push_counters[key]++;
            };
            $.each($(this).serializeArray(), function(){
                // skip invalid keys
                if(!patterns.validate.test(this.name)){
                    return;
                }
                var k,
                    keys = this.name.match(patterns.key),
                    merge = this.value,
                    reverse_key = this.name;
                while((k = keys.pop()) !== undefined){
                    // adjust reverse_key
                    reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');
                    // push
                    if(k.match(patterns.push)){
                        merge = self.build([], self.push_counter(reverse_key), merge);
                    }
                    // fixed
                    else if(k.match(patterns.fixed)){
                        merge = self.build([], k, merge);
                    }
                    // named
                    else if(k.match(patterns.named)){
                        merge = self.build({}, k, merge);
                    }
                }
                json = $.extend(true, json, merge);
            });
            return json;
        };
    })(jQuery);
}newJsonParse();