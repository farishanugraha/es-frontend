$('.fa').css('pointer-events', 'none');
var div = document.getElementById( 'editor' );
var editor = new Squire( div, {
  blockTag: 'p',
  blockAttributes: {'class': 'paragraph'},
  tagAttributes: {
      ul: {'class': 'UL'},
      ol: {'class': 'OL'},
      li: {'class': 'listItem'},
      a: {'target': '_blank'}
  }
});
Squire.prototype.makeHeader = function() {
    return this.modifyBlocks( function( frag ) {
      var output = this._doc.createDocumentFragment();
      var block = frag;
      while ( block = Squire.getNextBlock( block ) ) {
        output.appendChild(
          this.createElement( 'h2', [ Squire.empty( block ) ] )
        );
      }
      return output;
    });
};
document.addEventListener( 'click', function ( e ) {
    var id = e.target.id,
        value;
    var color = '#999';
    if ( id && editor && editor[ id ] ) {
        if ( e.target.className === 'prompt' ) {
            value = prompt( 'Value:' );
        }

        if(id=='bold'){
            editor[ id ]( value );
            $('#bold').css('color', 'black');
            $('#bold').attr('id', 'removeBold');
        }else if(id=='removeBold'){
            editor[ id ]( value );
            $('#removeBold').css('color', color);
            $('#removeBold').attr('id', 'bold');
        }

        else if(id=='italic'){
            editor[ id ]( value );
            $('#italic').css('color', 'black');
            $('#italic').attr('id', 'removeItalic');
        }else if(id=='removeItalic'){
            editor[ id ]( value );
            $('#removeItalic').css('color', color);
            $('#removeItalic').attr('id', 'italic');
        }

        else if(id=='underline'){
            editor[ id ]( value );
            $('#underline').css('color', 'black');
            $('#underline').attr('id', 'removeUnderline');
        }else if(id=='removeUnderline'){
            editor[ id ]( value );
            $('#removeUnderline').css('color', color);
            $('#removeUnderline').attr('id', 'underline');
        }


        else{
            editor[ id ]( value );
        }

    }
}, false );
$('#submit_editor').click(function(e){
    e.preventDefault();
    var msg = $('#editor').html();
    var data = {
        'id': curUser.uid,
        'message': msg
    }

    console.log(data)

    $.ajax({
        type: 'POST',
        url: url.feedback,
        data: {
            'id': data.id, 'message' : data.message
        },
        success: function(res){
            console.log(res)
            console.log('SUKSESSSS')
            alertCustom('success', 'Pesan terkirim!', 0);
            setTimeout(function(e){
                window.location = '/main';
            }, 3000);
        },
        error: function(res){
            alertCustom('error', 'Pesan belum berhasil terkirim. '+res.status, 0)
        }
    })
})
