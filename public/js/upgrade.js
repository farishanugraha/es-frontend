//GET URL
var curUrl = String(window.location.pathname);
var upgradeThis = curUrl.substr(9);

if (curUser==null){
	var upgradeCode = $('#upgrade_id').val();
	curUser={
		"uid":upgradeCode
	}
	console.log('%c curUser.id = null\n;USING UPGRADECODE:\n'+
		upgradeCode, 'color:orange')
}else{
	console.log("upgrade this: "+upgradeThis)
}

//FILTER
// console.log(curUser.uid, upgradeCode)
if(upgradeThis==curUser.uid){
	$('.upgrade').removeClass('hide');
}else if(upgradeThis==upgradeCode) {
	$('.upgrade').removeClass('hide');
}
else {alertCustom('fail','link tidak valid')}

$('.upgrade').hover3d({
    selector: "#form_upgrade",
    sensitivity: 200
});

//UPLOAD IMG
newJsonParse();
$('#submit_upgrade').click(function(){
	var dataSubmited  = $('#form_upgrade').serializeObject();
	
	console.log('upload to:'+url.upload);
	alertCustom('loading','Tunggu sebentar. File sedang diupload.',0)

	var form = new FormData($('#form_upgrade')[0]);
	console.log(form)
	// FUNGSI BARU
	$.ajax({
		method: "POST",
		url: url.upload,
		data: form,
		dataType: 'json',
		contentType: false,
		processData: false,
		success: function(data){
			console.log(data)
			alertCustom('success','Bukti transfer berhasil diupload. Silahkan cek email.<span id="alertCount"></span>',0)
			var count = 0;
			setTimeout(function(){
				count += 1;
				// window.location  = '/';
				$('#alertCount').html(count)
			}, 5000);

		},
		error: function(err){
			console.log('error')
			alertCustom('error','Bukti transfer gagal diupload. Silahkan coba kembali.',0)
		}
	});

});