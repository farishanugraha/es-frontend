$(document).ready(function(){

    /*========================//
    //       SUBMIT LOGIN     //
    //========================*/
    window.addEventListener('keypress', function (e) {
        var key = e.which || e.keyCode;
        if (key === 13) { // 13 is enter
          $('#submit_login').click();
        }
    });

    $('#submit_login').click(function(e){
        submitFormLogin();
        return false;
    });
    $('#submit_login').submit(function(e){
        submitFormLogin();
        return false;
    });

    $('#submit_forgotpass').click(function(e){
        submitFormForgot();
        return false;
    });

    $('#submit_forgotpass').submit(function(e){
        submitFormForgot();
        return false;
    });

    $('#form_forgotpass').validate({
        rules:{
            email:{required: true, email: true}
        },
        messages:{
            email:{required: "Masukkan email yang valid", email: "Masukkan email yang valid"}
        }
    });

    $('#form_login').validate({
        rules:{
            email:{required: true, email: true},
            password:{required: true, minlength: 3}
        },
        messages:{
            email:{required: "Masukkan email yang valid", email: "Masukkan email yang valid"},
            password:{required: "Masukkan password", minglength: "Minimal 3 karater"}
        }
    });
});

function submitFormLogin(){
    if($('#form_login').valid()==true){
            var dataSubmited = $('#form_login').serializeObject();
            $.ajax({
                type: "POST",
                url: url.login,
                data: {
                    "email":dataSubmited.email,
                    "password":dataSubmited.password
                },
                success: function(data) {

                    //=======================================================
                    // ALERT
                    if (dataSubmited.email=='yoyoyo@yoyoyo.com') {
                        alertCustom('success', 'Halo mas Erix!',0);
                    }else{
                        alertCustom('success', 'Selamat Datang!',0);
                    }

                    var dingdong = new Audio('/sound/dingdong.wav');
                    dingdong.volume = 0.3;
                    dingdong.play();

                    // SET AUTH
                    localStorage.setItem('isAuthed',true);

                    // SET, CHECK, GET : TOKEN
                    // SET USER
                    localStorage.setItem('uToken',data.token);
                    if(localStorage.uToken == data.token) 
                        {getToken();}
                    else
                        {console.log('wrong token');}

                    // REDIRECT HOME
                    setTimeout(function(){
                        $('#flashMessage').hide();
                        window.location.href = '/';
                    }, 1500);
                    //=======================================================

                },
                error: function(err){                
                    //ALERT
                    alertCustom('error', 'Email/Password salah.',0);
                    setTimeout(function(){
                      $('#flashMessage').hide();
                    },5000);

                    //FORM RESET
                    $('#emailLogin').focus();
                    document.getElementById("form_login").reset();
                    $('input').css('border-bottom', '1px solid #aaa');
                    console.log($('input'))

                    //LOG RESULT
                    console.log(err);
                }
            });
        }else{ alertCustom('error', 'Maaf, form tidak valid.', 3000); }
}

function submitFormForgot(){
    if($('#form_forgotpass').valid()==true){
        var data = $('#form_forgotpass').serializeObject();
        console.log(data)
        $.ajax({
            type: 'POST',
            url: url.forgot,
            data: data,
            success: function(res){
                console.log(res)
                console.log('SUKSESSSS')
                alertCustom('success', 'Data berhasil dikirim. Silahkan cek email.', 0);
                setTimeout(function(e){
                    window.location = '/login';
                }, 3000);
            },
            error: function(res){
                console.log(res)
                alertCustom('error', res.responseJSON.status, 3000);
            }
        })
    } else{
        alertCustom('error', 'Form tidak valid', 3000);
    }
}

$('input').keypress(function(e){
    if(e.target.value.length>=3 && e.target.name != 'email'){
        this.style.borderBottom = '1px solid green';
    }else if(e.target.value.indexOf('@')>-1 && e.target.value.indexOf('.')>-1 && e.target.name == 'email'){
        this.style.borderBottom = '1px solid green';
    }
    else{
        this.style.borderBottom = '1px solid red';
    }
})

$('input').blur(function(e){
    if(e.target.className=='valid'){
        this.style.borderBottom = '1px solid green';
    }else{
        this.style.borderBottom = '1px solid red';
    }
})