$(document).ready(function(e){
    window.addEventListener('keypress', function (e) {
        var key = e.which || e.keyCode;
        if (key === 13) { // 13 is enter
          $('#submit_resetpass').click();
        }
    });

    $('#submit_resetpass').click(function(e){
        submitFormReset();
        return false;
    });

    $('#submit_resetpass').submit(function(e){
        submitFormReset();
        return false;
    });

    $('#form_resetpass').validate({
        rules:{
            password:{required: true, minlength: 3},
            password_confirm:{required: true, equalTo: password}
        },
        messages:{
            password:{required: "Masukkan kata sandi baru", minlength: "Minimal 3 karakter"},
            password_confirm:{required: "Masukkan konfirmasi kata sandi", equalTo: "Konfirmasi kata sandi salah"}
        }
    });
})

function submitFormReset(){
    if($('#form_resetpass').valid()==true){
        var data = $('#form_resetpass').serializeObject();
        // console.log(data)
        $.ajax({
            type: 'POST',
            url: url.resetpass,
            data: {'id': data.id, 'password' : data.password},
            success: function(res){
                console.log(res)
                console.log('SUKSESSSS')
                alertCustom('success', 'Kata sandi berhasil diganti.', 0);
                setTimeout(function(e){
                    window.location = '/login';
                }, 3000);
            },
            error: function(res){
                console.log(res)
            }
        })
    } else{
        alertCustom('error', 'Form tidak valid', 3000);
    }
}