var thisURL = url_wp_es.guest;

// var totalDataContainer = $('#totalpost');
var allDataContainer = $('#alldata');
var titleContainer = $('#title');
var dateContainer = $('#date');
var contentContainer = $('#content');

$(document).ready(function(){
	showData('latest')
	getAllData();
	dataController();
});

function showData(id){
	if(id=='latest'){
		getData(thisURL, function(json){	
			var data = json[8];
			var date = data.date.substr(0, 10);
			titleContainer.html(data.title.rendered);
			dateContainer.html('<i>'+date+'</i>');
			contentContainer.html(data.content.rendered);
		})
	}
	else{
		getData(url_wp_es.posts+id, function(data){
			titleContainer.html(data.title);
			// dateContainer.html('<i>tanggal: '+data.date+'</i>');
			if(data.content==undefined){
				contentContainer.html(data);
			}else{
				titleContainer.html(data.title.rendered);
				contentContainer.html(data.content.rendered);
			}
		})
	}

	getData(thisURL, function(json){
		// totalDataContainer.html('total post: '+json.length);	
	});
}

function getAllData(){
	getData(thisURL, function(json){
			for (var i = 0; i <= json.length - 1; i++) {
				var data = json[i];
				var id = data.id;
				var title = data.title.rendered;
				var excerpt = data.excerpt.rendered;
				var list = '<div class="article"><h4 id="'+id+'">'+title+'</h4><p></p></div>';
					allDataContainer.append(list)
			}
		})
}

function dataController(){
	allDataContainer.click(function(e){
		$('#alldata').removeClass('show');
		showData(e.target.id);
		$('#content').scrollTop(0);
	});
}

$(document).ready(function(e){
	// GUEST
	$('#room_guest_trigger, #close').click(function(e){
		e.stopPropagation();
	    $('#maincontent').toggle('fast');
	    $('.svg-container').toggleClass('blur');
	    $('#profile_container').hide();
	})

	$('#alldata_trigger').click(function(e){
		$('#alldata').toggleClass('show');
	})

	$('#close').click(function(e){
		$('#alldata').removeClass('show');
	})

	$('#komp').click(function(e){
		// e.stopPropagation();
		// $('#upload_popup').toggle('fast');
		window.location = '/guest/editor';
	})

	$('#upload').click(function(e){
		var data = {
			'name': curUser.name,
			'content' : $('#upload_content').val()
		}

		console.log(data);
		if(data.content.length>10){
			alertCustom('success', 'Karya/ide kamu terkirim!', 0);
			setTimeout(function(e){
				window.location.reload();
			}, 3000);
		}else{
			alertCustom('error', 'Karya/Idemu kurang panjang :(', 0);
			setTimeout(function(e){
				window.location.reload();
			}, 3000);
		}
	})

	$('#mirror').click(function(e){
		$('#profile_container').toggle('fast');
	})

	$('#guest_to_map').click(function(e){
	    window.location = '/';
	})

	$('#door').hover(function(e){
		$('#flashInfo').html('Coming Soon !')
		$('#flashInfo').show();
	}, function(e){
		$('#flashInfo').hide();
	})
})
