$(window).on('load', function(){
    $('#load').fadeOut('slow');
    setTimeout(function(){
        $('#load').remove();
    }, 3000);
});

var thisURL = url_wp_es.children;

// var totalDataContainer = $('#totalpost');
var allDataContainer = $('#alldata');
var titleContainer = $('#title');
var dateContainer = $('#date');
var contentContainer = $('#content');

$(document).ready(function(){
	showData('latest')
	getAllData();
	dataController();
});

function showData(id, fam_id){
	if(id=='latest'){
		getData(thisURL, function(json){	
			var data = json[2];
			var date = data.date.substr(0, 10);
			titleContainer.html(data.title.rendered);
			dateContainer.html('<i>'+date+'</i>');
			contentContainer.html(data.content.rendered);
		})
	}
	else if(id=='fam'){
		$('#fam_pic').html('');
		$('#fam_title').html('');
		$('#fam_content').html('');
		if(fam_id=='fam3'){
			getData(url_wp_es.posts+229, function(data){
				$('#fam_pic').html('<img src="'+family_member[2].pic+'"/>');
				$('#fam_title').html(data.title.rendered);
				$('#fam_content').html(data.content.rendered);
			})
		}else if(fam_id=='fam4'){
			getData(url_wp_es.posts+237, function(data){
				$('#fam_pic').html('<img src="'+family_member[3].pic+'"/>');
				$('#fam_title').html(data.title.rendered);
				$('#fam_content').html(data.content.rendered);
			})
		}else if(fam_id=='fam5'){
			getData(url_wp_es.posts+239, function(data){
				$('#fam_pic').html('<img src="'+family_member[4].pic+'"/>');
				$('#fam_title').html(data.title.rendered);
				$('#fam_content').html(data.content.rendered);
			})
		}else{}
	}
	else{
		getData(url_wp_es.posts+id, function(data){
			titleContainer.html(data.title);
			// dateContainer.html('<i>tanggal: '+data.date+'</i>');
			if(data.content==undefined){
				contentContainer.html(data);
			}else{
				titleContainer.html(data.title.rendered);
				contentContainer.html(data.content.rendered);
			}
		})
	}

	getData(thisURL, function(json){
		// totalDataContainer.html('total post: '+json.length);	
	});
}

function getAllData(){
	getData(thisURL, function(json){
			for (var i = 0; i <= json.length - 1; i++) {
				var data = json[i];
				var id = data.id;
				var title = data.title.rendered;
				var excerpt = data.excerpt.rendered;
				console.log(data.id)
				if(data.id==229 || data.id==237 || data.id==239){
				}else{
					var list = '<div class="article"><h4 id="'+id+'">'+title+'</h4></div>';
					allDataContainer.append(list)
				}
			}
		})
}

function dataController(){
	allDataContainer.click(function(e){
		$('#alldata').removeClass('show');
		showData(e.target.id);
		$('#content').scrollTop(0);
	});
}

// CHILDREN
$('#article, #close').click(function(e){
	e.stopPropagation(e);
    $('#maincontent').toggle('fast');
    $('.svg-container').toggleClass('blur');
    $('.svg-container').css('pointer-events', 'none');
    $('.family-content').hide();
})

$('.fam-trigger').click(function(e){
	e.stopPropagation();
	$('#maincontent').hide('fast');
	$('#familycontent').toggle('fast');
	$('.svg-container').toggleClass('blur');
	$('.svg-container').css('pointer-events', 'none');
	setTimeout(function(){
		showData('fam', e.target.id);
	}, 100);
})

$('#alldata_trigger').click(function(e){
	$('#alldata').toggleClass('show');
})

$('#close').click(function(e){
	$('#alldata').removeClass('show');
	$('.svg-container').css('pointer-events', 'initial');
})

$('#close_fam').click(function(e){
	$('#maincontent').hide('fast');
    $('.svg-container').removeClass('blur');
    $('.family-content').hide();
    $('.svg-container').css('pointer-events', 'initial');
})

$('#close_vid').click(function(e){
	$('#maincontent').hide();
	$('#videocontent').hide('fast');
    $('.svg-container').removeClass('blur');
    $('.family-content').hide();
    $('.svg-container').css('pointer-events', 'initial');
})

$('.children-trigger').click(function(e){
	alertCustom('loading', 'Coming Soon! Tunggu Daddy beli mainan baru ya!', 1500);
})

$('#video').click(function(e){
	e.stopPropagation();
    $('#videocontent').toggle('fast');
	$('#maincontent').hide();
    $('#familycontent').hide();
    $('.svg-container').toggleClass('blur');
    $('.svg-container').css('pointer-events', 'none');
})