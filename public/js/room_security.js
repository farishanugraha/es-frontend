window.addEventListener('keypress', function (e) {
    var key = e.which || e.keyCode;
    if (key === 13) { // 13 is enter
        if($('#search_this').is(':focus')){
          $('#search_on_site').click();
        }else{}
    }
});

// SECURITY
$('#room_security_trigger, #close').click(function(e){
	e.stopPropagation();
    $('#maincontent').toggle('fast');
    $('.svg-container').toggleClass('blur');
    $('.search-container').hide();
})

$('#search_trigger').click(function(e){
    e.stopPropagation();
    $('#maincontent').hide('fast');
    $('.svg-container').removeClass('blur');
	$('.search-container').toggle('fast');
    $('#search_this').focus();
})

$('#search_on_site').click(function(e){
    $('#search_result').html(loader);
    var input = $('#search_this').val();
    var result = [];
    getData(url_wp_es.posts+'?per_page=100', function(json){

        for (var i = 0; i < json.length; i++) {
            var data = json[i];
            var keyword = input.toLowerCase();
            var d_title = data.title.rendered.toLowerCase();
            var d_content = data.content.rendered.toLowerCase();
            console.log(data.title.rendered)
            console.log(wp_categories[data.categories])
            if(d_title.indexOf(keyword)>-1 || d_content.indexOf(keyword)>-1){
                result.push({
                    'title': data.title.rendered,
                    'categories': wp_categories[data.categories[0]]
                });
            }else{}
        }
        if(result.length>0){
            $('#search_result').html("");
            for (var i = 0; i < result.length; i++) {
                var res = result[i];
                if(res.categories=='bands'){
                    res.categories='guest';
                }else{}
                $('#search_result').append('<a href="/'+res.categories+'">'+res.title+'</a><br><br>');
            }
        }else{
            $('#search_result').html('Tidak ada artikel yang mengandung kata '+input+'.');
        }
        $('#search_result').css('border-top', '1px solid black');
        console.log('SELESAI')
    })
})