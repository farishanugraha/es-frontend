var thisURL = url_wp_es.amphi;

// var totalDataContainer = $('#totalpost');
var allDataContainer = $('#alldata');
var titleContainer = $('#title');
var dateContainer = $('#date');
var contentContainer = $('#content');

$(document).ready(function(){
	showData('latest')
	getAllData();
	dataController();
});

function showData(id){
	if(id=='latest'){
		getData(thisURL, function(json){	
			var data = json[0];
			var date = data.date.substr(0, 10);
			titleContainer.html(data.title.rendered);
			dateContainer.html('<i>'+date+'</i>');
			contentContainer.html(data.content.rendered);
		})
	}
	else{
		getData(url_wp_es.posts+id, function(data){
			titleContainer.html(data.title);
			// dateContainer.html('<i>tanggal: '+data.date+'</i>');
			if(data.content==undefined){
				contentContainer.html(data);
			}else{
				titleContainer.html(data.title.rendered);
				contentContainer.html(data.content.rendered);
			}
		})
	}

	getData(thisURL, function(json){
		// totalDataContainer.html('total post: '+json.length);	
	});
}

function getAllData(){
	getData(thisURL, function(json){
			for (var i = 0; i <= json.length - 1; i++) {
				var data = json[i];
				var id = data.id;
				var title = data.title.rendered;
				var excerpt = data.excerpt.rendered;
				var list = '<div class="article"><h4 id="'+id+'">'+title+'</h4></div>';
					allDataContainer.append(list)
			}
		})
}

function dataController(){
	allDataContainer.click(function(e){
		$('#alldata').removeClass('show');
		showData(e.target.id);
		$('#content').scrollTop(0);
	});
}

$('#article, #close').click(function(e){
	e.stopPropagation();
    $('#maincontent').toggle('fast');
    $('.svg-container').toggleClass('blur');
    $('.webcam-container').hide();
    stopRecording();
})

$('#alldata_trigger').click(function(e){
	$('#alldata').toggleClass('show');
})

$('#close').click(function(e){
	$('#alldata').removeClass('show');
})


// WEBCAM
var video, stream, recorder;
video = document.querySelector('video');
var recordedChunks = [];

$('#webcam_trigger').click(function(e){
	$('#maincontent').hide();
	$('.webcam-container').toggle();
	$('.svg-container').toggleClass('blur');
	resetWebcam();
})
$('#start_rec').click(function(e){
	startRecording(stream);
})
$('#stop_rec').click(function(e){
	stopRecording(stream);
})
$('#reset_rec').click(function(e){
	resetWebcam();
})

navigator.getUserMedia = ( navigator.getUserMedia ||
                       navigator.webkitGetUserMedia ||
                       navigator.mozGetUserMedia ||
                       navigator.msGetUserMedia);
function update(strm) {document.querySelector('video').src = strm.url;}
function hasGetUserMedia() {return !!(navigator.getUserMedia || navigator.webkitGetUserMedia);}
var errorCallback = function(e) {console.log('error', e)}
var successCallback = function(localMediaStream){
	stream = localMediaStream;
	video.volume = 0;
	video.src = window.URL.createObjectURL(localMediaStream);
	video.onloadedmetadata = function(e){
		// console.log(e)
	}
}
function showWebcam() {(hasGetUserMedia) ? navigator.getUserMedia(
	{
		video: {
			mandatory: {
				minWidth: 1280,
				minHeight: 720
			}
		}
	, audio: true}
	, successCallback, errorCallback) : alertCustom('error','Browser anda tidak mendukung fitur webcam.', 0);}

var timer = 0;
var count;

function resetWebcam(){
	timer = 0;
	document.getElementById('start_rec').removeAttribute('disabled');
	video.removeAttribute('controls');
	video.setAttribute('autoplay', true);
	video.load();
	showWebcam();
}

function startRecording(localMediaStream){
	console.log('start record')
	document.getElementById('start_rec').setAttribute('disabled', '');
	document.getElementById('reset_rec').setAttribute('disabled', '');
	document.getElementById('stop_rec').removeAttribute('disabled');
	$('.svg-container').css('pointer-events', 'none');
	showWebcam();
	// recordedChunks = [];
	recorder = new MediaRecorder(localMediaStream, {
		mimeType: 'video/webm'
	});
	recorder.ondataavailable = handleDataAvailable;
	recorder.start();
	recorder.onstart = function(){
		count = setInterval(function(e){
			timer+=1;
			$('#timer').html('<b>00:'+(60-timer)+'</b>')
			console.log('timer: '+ timer)
			if(timer==60){
				clearInterval(count);
				$('#stop_rec').click();
			}
		}, 1000);
	}
}

function stopRecording(){
	if(recorder.state=='inactive'){
		console.log('inactive state')
	}else{
		recorder.stop();
	}
	document.getElementById('start_rec').setAttribute('disabled', '');
	document.getElementById('reset_rec').removeAttribute('disabled');
	recorder.onstop = function(){
		console.log('stop record');
		clearInterval(count);
		$('#timer').html('');
		$('.svg-container').css('pointer-events', 'initial');
	}
}

function handleDataAvailable(e){
	if(e.data.size > 0) {
		recordedChunks.push(e.data);
		console.log(recordedChunks)
		var superBuffer = new Blob(recordedChunks);
		video.src = window.URL.createObjectURL(superBuffer);
		video.volume = 1;
		video.setAttribute('controls', '');
		video.setAttribute('controlsList', 'nodownload');
		video.removeAttribute('autoplay');

		$('#blob_container').html('');
		for (var i = 0; i < recordedChunks.length; i++) {
			console.log(recordedChunks[i])
			var blob = new Blob(recordedChunks, {type: 'video/webm'});
			var url = URL.createObjectURL(blob);
			$('#blob_container').append(
				'<li><b>TAKE #'+ (i+1) +'</b> ('+recordedChunks[i].size/1000+'kb)<br>'
				+' <a href="'+url+'" download="test.webm"><button>download</button></a>'
				+' | <button id="'+i+'" onclick="uploadThis(this.id)">upload</button>'
				+'</li>')	
		}


	}else{
		alertCustom('loading', 'kosong', 3000);
	}
}

function uploadThis(id){
	console.log(recordedChunks[id])


	// UPLOAD VIDEO DISINI

	// $.ajax({
 //        type: 'POST',
 //        url: url.video,
 //        data: recordedChunks[id],
 //        success: function(res){
 //            console.log(res)
 //        },
 //        error: function(res){
 //            console.log(res)
 //            alertCustom('error', res.responseJSON.status, 3000);
 //        }
 //    })
}