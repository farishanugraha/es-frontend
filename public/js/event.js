toggleEvPage(1);toggleEvPage(2);
toggleEvPage(3);toggleEvPage(4);
toggleEvPage(5);toggleEvPage(6);
toggleEvPage(7);

$('.nav ul li').click(function(){
	$('.nav ul li').removeClass('selected');
    $(this).addClass('selected');
});

$('#form_ev').validate({
	rules:{valid:'required'},
	messages:{valid:'required'}
});

$('#submit_ev').click(function(){
    if($('#form_ev').valid()==true){
	    var dataSubmited = $('#form_ev').serializeObject();
	    $('.alert').hide();
		alert(dataSubmited)
	    window.location.href = '/confirm';		
    }else{
    	console.log('not valid')
    }

    return false;
});

function toggleEvPage(id){
	$('#e'+id).click(function(){
		$('.event-page').hide();
		$('#ep'+id).show('fast');
	});
};

$('.cal-trigger').click(function(e){
	e.stopPropagation();
	console.log()
	showEvent(e.target.id.substr(2))
})

function showEvent(id){
	var data = event_list[id-1];
	$('#title').html(data.title);
	$('#ep1').html(data.about);
	$('#ep2').html(data.venue);
	$('#ep3').html(data.schedule);
	$('#ep4').html(data.speakers);

	if(data.gallery!='-'){
		$('#ep5').html('<img src="'+data.gallery+'" alt="-"/>');
	}else{
		$('#ep5').html('-');
	}
	
	$('#ep6').html(data.details);
	$('.event').toggle('fast');
	$('.svg-container').toggleClass('blur');
}

$('#close').click(function(){
	$('.event').hide();
	$('.svg-container').removeClass('blur');
})