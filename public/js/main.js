$(window).on('load', function(){
    $('#load').fadeOut('slow');
    setTimeout(function(){
        $('#load').remove();
    }, 3000);
});

$(document).ready(function(e){
    browserDetection();
    checkAuth();
    getToken();
    roomControl();
    logout();

    if(window.location.pathname=='/'){
        $('#map_trigger').hide('slow');
    }

    // HIDE zoom trigger di beberapa page
    if(
        window.location.pathname.substr(0, 5) == '/main' ||
        window.location.pathname == '/login' ||
        window.location.pathname == '/register' ||
        window.location.pathname == '/forgot_pass' ||
        window.location.pathname == '/does'
    ){
        $('.zoom-trigger').hide();
    }else{
        // console.log(window.location.pathname.substr(0, 5))
        $('.zoom-trigger').show();
    }
    
    $('.svg-container').scrollTop(100);
    $('.svg-container').scrollLeft(200);
});

var about = 'Situs pribadi pertama di Indonesia yang mengadopsi konsep bangunan nyata dengan teknologi tiga dimensi interaktif dan ilustrasi ruang perspektif.';
var about_arr;
var counter = 0;

$('#toggle_cloud_about').click(function(e){
    counter+=1;
    if(counter%2==0){
        $('#cloud_about').toggle();
        $('.cloud-footer').toggle('fast');
    }else{
        about_arr = [];
        about_arr = about.split('');
        $('#anim_about').html('');

        $('#cloud_about').toggle();
        $('.cloud-footer').toggle('fast');

        var i = 0;
        var typer = setInterval(function(e){
            $('#anim_about').append(about_arr[i])
            i+=1;
            if(i>=about_arr.length){
                clearInterval(typer);
                $('#toggle_cloud_about').prop('disabled', false);
            }else{
                $('#toggle_cloud_about').prop('disabled', true);
            }
        }, 20);
    }
});

$('.svg-container').click(function(e){
    $('#maincontent').hide('fast');
    $('#upload_popup').hide('fast');
    $('.svg-container').removeClass('blur');
    
    $('.family-content').hide();
    
    // security
    $('.search-container').hide();

    // office
    $('.office-content').hide();

    // event
    $('.event').hide();

    // amphi
    $('.webcam-container').hide();

    // guest
    $('#profile_container').hide();

    // children
    $('#videocontent').hide()
});

$('.page-trigger').click(function(e){
    e.stopPropagation();
});

$( '#zoomIn' ).click(function(e) {
    scaling('in');
});

$( '#zoomOut' ).click(function(e) {
    scaling('out');
});

$('.main-menu-trigger').hover(function(e){
    if(e.target.id=='logout' || e.target.id=='logout_trigger'){
        $('.logout-trigger-info').show();
    }else if(e.target.id=='map_link' || e.target.id=='map_trigger' || e.target.id=='map_trigger_img'){
        $('.map-trigger-info').show();
    }
    else if(e.target.id=='tm' || e.target.id=='tm_img'){
        $('.back-trigger-info').show();
    }
}, function(e){
    $('.trigger-info').hide();
});

$('.to-main-trigger').hover(function(e){
    $('.back-trigger-info').show();
}, function(e){
    $('.trigger-info').hide();
});

$('#tips').click(function(e){
    $('#tips').html("Drag (klik tahan) untuk menjelajah ruangan ;)");
});

// play/pause video by klik
$('video').click(function(e){
    var myVid = document.querySelector('video');
    if(myVid.paused == false){
        myVid.pause();
        myVid.firstChild.nodeValue = 'Play';
    }else{
        myVid.play();
        myVid.firstChild.nodeValue = 'Pause';
    }
})

if(window.location.pathname == '/'){
    // hover3d
    $('.cloud').hover3d({
        selector: "#brand_img",
        sensitivity: 50
    });
}