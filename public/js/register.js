$(document).ready(function(){
    $('#submit_reg').html('Daftar')
    $('#password').click(function(){
        var nama = $('#name').val();
        $('#submit_reg').html('Saya <span id="nama"></span>, mas.')
        $('#nama').html(nama)
    });
    $.getJSON(urlProv, function(json) {
      var data = json;
        for (i = 0; i < data.rajaongkir.results.length; i++) {
            provinces = '<option value="'+data.rajaongkir.results[i].province_id+'">'+data.rajaongkir.results[i].province+'</option>';
            $('#regProvince').append(provinces);
        }
        $('#regProvince').on('change', function(e){
            $('#regCity').empty();
            var optionSelected = $("option:selected", this);
            var valueSelected = this.value;
            province = '<input type="hidden" name="province_name" value="'+ data.rajaongkir.results[valueSelected-1].province+'">'
            $('#regProvinceName').html(province)
            // console.log(province)
            for(var j=0; j < data.rajaongkir.results.length;j++){
                if (valueSelected == data.rajaongkir.results[j].province_id) {
                    urlCity = urlCityRaw + data.rajaongkir.results[j].province_id;
                    $.getJSON(urlCity, function(json){
                        var data = json;
                        for (var k = 0; k < data.rajaongkir.results.length; k++) {
                            cities = '<option value="'+data.rajaongkir.results[k].city_id+'">'+data.rajaongkir.results[k].type+' '+data.rajaongkir.results[k].city_name+'</option>';
                            $('#regCity').append(cities);
                        }
                        $('#regCity').on('change', function(){
                            var optionSelected = $("option:selected", this);
                            var valueSelected = this.value;
                            console.log(valueSelected);
                            console.log(data.rajaongkir.results)

                            for (var i = data.rajaongkir.results.length - 1; i >= 0; i--) {
                                if(valueSelected==data.rajaongkir.results[i].city_id){
                                    // console.log(data.rajaongkir.results[i].city_name)
                                    var city_name = '<input type="hidden" name="city_name" value="'+ data.rajaongkir.results[i].city_name+'">'
                                    $('#regCityName').html(city_name)
                                    // console.log(city_name)
                                }
                            }
                        });
                    });
                }
            }   
        });
    });

    $('#form_reg').validate({
        rules:{
            password:{required: true, minlength: 6},
            name:{required: true, minlength:3},
            address:"required",
            // city:"required",
            phone:"required",
            interest:"required",
            birthdate:{required: true, date: true},
            email:{required: true, email: true}
        },
        messages:{
            password:{required: "Masukkan password", minlength: "Minimal 6 karakter"},
            name:{required: "Masukkan nama", minlength: "Minimal 3 karakter"},
            street:"Masukkan alamat",
            // city:"Masukkan domisili",
            phone:"Masukkan nomor telepon",
            interest:"Masukkan minat",
            profession:"Masukkan profesi",
            birthdate:{required: "Masukkan tanggal lahir", date: "Masukkan tanggal lahir"},
            email:{required: "Masukkan email yang valid", email: "Masukkan email yang valid"}
        }
    });

    $('#submit_reg').click(function(){
        if($('#form_reg').valid()==true){
            var dataSubmited = $('#form_reg').serializeObject();
            console.log(dataSubmited);
            alertCustom('loading','Permintaan anda sedang diproses...', 0);
            $.ajax({
              method: "POST",
              url: url.register,
              data: {
                "name":dataSubmited.name,
                "password":dataSubmited.password,
                "email":dataSubmited.email,
                "phone":dataSubmited.phone,
                "interest":[dataSubmited.interest],
                "profession":dataSubmited.profession,
                "date_birth":dataSubmited.birthdate,
                "address":{
                    "province_id":dataSubmited.province,
                    "province_name":dataSubmited.province_name,
                    "city_id":dataSubmited.city,
                    "city_name":dataSubmited.city_name,
                    "street":dataSubmited.street    },
                "role":{
                    "id":1
                }
              },
              success:function(data){
                console.log(this.data)
                alertCustom('success','Pendaftaran berhasil!',0);
                setTimeout(function(){
                    window.location = '/login';
                    $('#flashMessage').hide();
                }, 3000);
              },
              error:function(err){
                alertCustom('error','Email sudah digunakan.',3000);
                console.log(err);
              } 
            });
        }else{
            alertCustom('error','Hmm. Ada yang aneh..', 2000);
            $('.error').css('border-bottom: 1px solid #800000')
        }
    });
});

var regY = TODAY_DATE.substr(0,4) - 3;
var regM = TODAY_DATE.substr(5,2);
var regD = TODAY_DATE.substr(8,2);
var regDT = regY+'-'+regM+'-'+regD;

$('#birthdate').attr('max', regDT);

$('input').keypress(function(e){
    if(e.target.value.length>=3 && e.target.name != 'email'){
        this.style.borderBottom = '1px solid green';
    }else if(e.target.value.indexOf('@')>-1 && e.target.value.indexOf('.')>-1 && e.target.name == 'email'){
        this.style.borderBottom = '1px solid green';
    }
    else{
        this.style.borderBottom = '1px solid red';
    }
})

$('input').blur(function(e){
    if(e.target.className=='valid'){
        this.style.borderBottom = '1px solid green';
    }else{
        this.style.borderBottom = '1px solid red';
    }
})