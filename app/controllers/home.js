const express = require('express');
const router = express.Router();
const Article = require('../models/article');

module.exports = (app) => {
  app.use('/', router);
};

router.get('/', (req, res, next) => {
  const articles = [new Article(), new Article()];
  res.render('index', {
    title: 'Erix Soekamti',
    scripts: '<script type="text/javascript" src="/js/hover3d.js"></script>'
  });
});

router.get('/comingsoon', function (req, res, next) {
  res.render('coming_soon', {
    title: 'Erix Soekamti',
    layout: 'coming_soon'
  });
});

router.get('/guest/editor', function (req, res, next) {
  res.render('editor', {
    title: 'Erix Soekamti',
    layout: 'editor'
  });
});

router.get('/main/editor', function (req, res, next) {
  res.render('editor', {
    title: 'Erix Soekamti',
    layout: 'editor'
  });
});

router.get('/login', function (req, res, next) {
    res.render('login', {
      title: 'LOGIN - ErixSoekamti.com',
      scripts:'<script type="text/javascript" src="/components/jquery-validation/dist/jquery.validate.min.js"></script>'+ 
              '</script><script type="text/javascript" src="/js/login.js"></script>'
    });
});

router.get('/register', function (req, res, next) {
    res.render('register', {
      title: 'REGISTER - ErixSoekamti.com',
      scripts:'<script type="text/javascript" src="/components/jquery-validation/dist/jquery.validate.min.js"></script>'+ 
              '</script><script type="text/javascript" src="/js/register.js"></script>'
    });
});

router.get('/forgot_pass', function (req, res, next) {
    res.render('login_forgotpass.handlebars', {
      title: 'FORGOT PASSWORD - ErixSoekamti.com',
      scripts:'<script type="text/javascript" src="/components/jquery-validation/dist/jquery.validate.min.js"></script>'+ 
              '<script type="text/javascript" src="/js/login.js"></script>'
    });
});

router.get('/reset-password/:id', function (req, res, next) {
    res.render('login_resetpass', {
      title: 'RESET - ErixSoekamti.com',
      scripts:'<script type="text/javascript" src="/components/jquery-validation/dist/jquery.validate.min.js"></script>'+ 
              '<script type="text/javascript" src="/js/login_resetpass.js"></script>',
      resetcode:req.params.id
    });
});

router.get('/upgrade/:id', function (req, res, next) {
    res.render('upgrade', {
      title: 'UPGRADE - ErixSoekamti.com',
      scripts:'<script type="text/javascript" src="/components/jquery-validation/dist/jquery.validate.min.js"></script>'+ 
              '<script type="text/javascript" src="/js/hover3d.js"></script>'+
              '<script type="text/javascript" src="/js/upgrade.js"></script>',
      upgradecode:req.params.id
    });
});

router.get('/does', function (req, res, next) {
    res.render('does', {
      title: 'DOES - ErixSoekamti.com',
      scripts: '<script type="text/javascript" src="/js/does.js"></script>'
    });
});

/*=========================*/
/*   ROOMS ROUTER          */
/*=========================*/

router.get('/security', function (req, res, next) {
  res.render('room_security', {
    title: 'INFORMASI - ErixSoekamti.com',
    scripts: '<script type="text/javascript" src="/js/room_security.js"></script>'
  });
});

router.get('/main', function (req, res, next) {
  res.render('room_main', {
    title: 'RUANG TAMU - ErixSoekamti.com',
    scripts:'<script type="text/javascript" src="/js/three.js"></script>'+
              '<script type="text/javascript" src="/js/three_controls.js"></script>'+
              '<script type="text/javascript" src="/js/3d_config.js"></script>'+
              '<script type="text/javascript" src="/js/3d_room_main.js"></script>'
  });
});

router.get('/children', function (req, res, next) {
    res.render('room_children', {
      title: 'ANAK - ErixSoekamti.com',
      scripts: '<script type="text/javascript" src="/js/room_children.js"></script>'
    });
});

router.get('/garage', function (req, res, next) {
    res.render('room_garage', {
      title: 'OTOMOTIF - ErixSoekamti.com',
      scripts: '<script type="text/javascript" src="/js/room_garage.js"></script>'
    });
});

router.get('/studio/music', function (req, res, next) {
    res.render('room_studio_music', {
      title: 'STUDIO MUSIK - ErixSoekamti.com',
      scripts: '<script type="text/javascript" src="/js/room_studio_music.js"></script>'
    });
});

router.get('/studio/video', function (req, res, next) {
    res.render('room_studio_visual', {
      title: 'STUDIO VISUAL - ErixSoekamti.com',
      scripts: '<script type="text/javascript" src="/js/room_studio_visual.js"></script>'
    });
});

router.get('/guest', function (req, res, next) {
    res.render('room_guest', {
      title: 'TAMU - ErixSoekamti.com',
      scripts: '<script type="text/javascript" src="/js/room_guest.js"></script>'+
                '<script type="text/javascript" src="/js/profile.js"></script>'
    });
});

router.get('/pool', function (req, res, next) {
    res.render('room_pool', {
      title: 'DIVING - ErixSoekamti.com',
      scripts: '<script type="text/javascript" src="/js/room_pool.js"></script>'
    });
});

router.get('/family', function (req, res, next) {
    res.render('room_family', {
      title: 'KELUARGA - ErixSoekamti.com',
      scripts: '<script type="text/javascript" src="/js/room_family.js"></script>'
    });
});

router.get('/personal', function (req, res, next) {
    res.render('room_personal', {
      title: 'AKU - ErixSoekamti.com',
      scripts: '<script type="text/javascript" src="/js/room_personal.js"></script>'
    });
});

router.get('/amphi', function (req, res, next) {
    res.render('room_amphi', {
      title: 'AMPHITHEATER - ErixSoekamti.com',
      scripts: '<script type="text/javascript" src="/js/room_amphi.js"></script>'
    });
});

router.get('/office', function (req, res, next) {
    res.render('room_office', {
      title: 'KANTOR - ErixSoekamti.com',
      scripts: '<script type="text/javascript" src="/js/room_office.js"></script>'
    });
});

router.get('/kitchen', function (req, res, next) {
    res.render('room_kitchen', {
      title: 'DAPUR - ErixSoekamti.com',
      scripts: '<script type="text/javascript" src="/js/room_kitchen.js"></script>'
    });
});

router.get('/garden', function (req, res, next) {
    res.render('room_garden', {
      title: 'TAMAN - ErixSoekamti.com',
      scripts: '<script type="text/javascript" src="/js/room_garden.js"></script>'
    });
});

router.get('/gym', function (req, res, next) {
    res.render('room_gym', {
      title: 'GYM - ErixSoekamti.com',
      scripts: '<script type="text/javascript" src="/js/room_gym.js"></script>'
    });
});

/*=========================*/
/* ROOMS ROUTER - 2nd level*/
/*=========================*/

// ----------- MAIN ---------------------------------------
router.get('/main/guestbook', function (req, res, next) {
    res.render('room_main_guestbook', {
      title: 'BUKU TAMU - ErixSoekamti.com',
      scripts: '<script type="text/javascript" src="/js/squire.js"></script>'+
                '<script type="text/javascript" src="/js/room_main_guestbook.js"></script>'

    });
});

router.get('/main/news', function (req, res, next) {
    res.render('room_main_news', {
      title: 'BERITA - ErixSoekamti.com',
      scripts: '<script type="text/javascript" src="/js/article.js"></script>'
    });
});

router.get('/main/gallery', function (req, res, next) {
    res.render('room_main_gallery', {
      title: 'GALERI - ErixSoekamti.com',
      scripts: '<script type="text/javascript" src="/js/article_gallery.js"></script>'
    });
});

router.get('/main/event', function (req, res, next) {
    res.render('room_main_event', {
      title: 'JADWAL - ErixSoekamti.com',
      scripts: '<script type="text/javascript" src="/components/jquery-validation/dist/jquery.validate.min.js"></script>'+
               '<script type="text/javascript" src="/js/event.js"></script>'
    });
});
// ---------------------------------------------------------